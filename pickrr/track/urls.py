from django.conf.urls import patterns, url
from track import views

urlpatterns = patterns('',
	url(r'^$',views.search_tracking,name='search_tracking'),
    url(r'^(?P<key>.+)/(?P<track_id>.+)/$',views.show_tracking,name='show_tracking'),
    url(r'^(?P<tracking_id>.+)/mobile/api$',views.tracking_api,name='tracking_api'),
)