from django import forms
from helpers.helpers import get_or_none
#from orders.models import Order
from track.models import Status

class TrackingForm(forms.Form):
    #email = forms.EmailField(label='Enter Checkout Email ID',required=True)
    tracking_id = forms.CharField(label='Enter Tracking ID',required=True)

    #Custom validation
    def clean(self):
        cleaned_data = super(TrackingForm, self).clean()
        tracking_id = cleaned_data.get("tracking_id")
        #order_id = cleaned_data.get("order_id")
        if tracking_id:
            track_obj = get_or_none(Status,tracking_id=tracking_id)
            if not track_obj:
                msg = u"Invalid Tracking ID"
                self._errors["tracking_id"] = self.error_class([msg])
                del cleaned_data["tracking_id"]
        return cleaned_data