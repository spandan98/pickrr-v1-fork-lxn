from django.contrib import admin
from track.models import Status

class TrackAdmin(admin.ModelAdmin):
    search_fields=('tracking_id','order__order_id')

admin.site.register(Status,TrackAdmin)