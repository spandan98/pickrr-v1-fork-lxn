from django.db import models
import random
from helpers.helpers import send_sms, clean_phone_number

class Status(models.Model):
    tracking_id = models.CharField(max_length=64,blank=True,null=True)
    order = models.OneToOneField('orders.Order',blank=True,null=True)
    #is_picked = models.BooleanField(default=False)
    pickup_time = models.DateTimeField(blank=True,null=True)
    warehouse_receive_time = models.DateTimeField(blank=True,null=True)
    in_transit_time = models.DateTimeField(blank=True,null=True)
    in_transit_status = models.TextField(blank=True,null=True)
    out_delivery_time = models.DateTimeField(blank=True,null=True)
    #intermediate_status = models.CharField(max_length=64,default='In Transit',blank=True,null=True)
    #is_delivered = models.BooleanField(default=False)
    delivery_date = models.DateTimeField(blank=True,null=True)
    key = models.TextField(blank=True,null=True)

    def __unicode__(self):
        return str(self.order.order_id)

    def save(self, *args, **kwargs):
        if not self.tracking_id:
            n = 8
            self.tracking_id = ''.join(["%s" % random.randint(0, 9) for num in range(0, n)])
        #Sending an sms on pickup along with an invoice
        if self.pk is not None:
            if self.pickup_time is not None and self.order.user.pickup_notification:
                track_obj = Status.objects.get(pk=self.pk)
                #First time entry of pickup time
                if track_obj.pickup_time is None:
                    destination_number = clean_phone_number(self.order.pickup_address.phone_number)
                    if self.order.total_bill > float(0):
                        total_bill = self.order.total_bill
                    else:
                        total_bill = None
                    first_line = 'Dear ' + self.order.pickup_address.name + ',' + '\n'
                    if self.order.user.email == 'contact@wishup.in':
                        second_line = ('Thank you for placing your order on Wishup.',
                                'Your parcel for the order',
                                '#%s'%self.order.order_id,
                                'has been successfully picked by our logistics partner Pickrr on',
                                self.pickup_time.strftime("%B %d, %y, %H:%M"))
                    else:
                        second_line = ('Your parcel for the order',
                                '#%s'%self.order.order_id,
                                'has been successfully picked by Pickrr on',
                                self.pickup_time.strftime("%B %d, %y, %H:%M"))
                    second_line = ' '.join(second_line)
                    second_line = second_line + '.' + '\n' + '\n'
                    fourth_line = ('You can track your shipment on http://www.pickrr.com via your tracking ID',
                                    self.tracking_id)
                    fourth_line = ' '.join(fourth_line)
                    fourth_line += '.'
                    if total_bill is not None:
                        third_line = ('Your Total invoice amount was',
                                    'Rs',
                                    str(total_bill))
                        third_line = ' '.join(third_line)
                        third_line = third_line + '.' + '\n' + '\n'
                        message = first_line + second_line + third_line + fourth_line
                    else:
                        message = first_line + second_line + fourth_line
                    send_sms(destination_number,message)
            #second sms on succesfull delivery
            if self.delivery_date is not None:
                track_obj = Status.objects.get(pk=self.pk)
                #First time entry of pickup time
                if track_obj.delivery_date is None:
                    if self.order.user.email == 'archies@pickrr.com':
                        destination_number = clean_phone_number(self.order.drop_address.phone_number)
                        first_line = 'Dear ' + self.order.drop_address.name + ',' + '\n'
                    else:
                        destination_number = clean_phone_number(self.order.pickup_address.phone_number)
                        first_line = 'Dear ' + self.order.pickup_address.name + ',' + '\n'
                    if self.order.user.email == 'contact@wishup.in':
                        second_line = ('Thanks for choosing Wishup.',
                                'Your parcel for the order',
                                '#%s'%self.order.order_id,
                                'has been successfully delivered by our logistics partner Pickrr on',
                                self.delivery_date.strftime("%B %d, %y, %H:%M"))
                    else:
                        second_line = ('Your parcel for the order',
                                '#%s'%self.order.order_id,
                                'has been successfully delivered by Pickrr on',
                                self.delivery_date.strftime("%B %d, %y, %H:%M"))
                    second_line = ' '.join(second_line)
                    second_line = second_line + '.'
                    third_line = 'Check out our android app at https://goo.gl/xR4U8e'
                    message = first_line + second_line + third_line
                    send_sms(destination_number,message)
        super(Status, self).save(*args, **kwargs)