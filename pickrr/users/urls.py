from django.conf.urls import patterns, url
from users import views

urlpatterns = patterns('',
    url(r'^signup/$',views.user_signup,name='user_signup'),
    url(r'^login/$',views.user_login,name='user_login'),
    url(r'^account/$',views.user_account,name='user_account'),
    url(r'^account/edit/$',views.user_account_edit,name='user_account_edit'),
    url(r'^(?P<user_id>.+)/(?P<auth_token>.+)/account-edit-api/$',views.mobile_edit_account,name='mobile_edit_account'),
    url(r'^logout/$',views.user_logout,name='user_logout'),
    url(r'^addresses/$',views.view_add_address,name='view_add_address'),
    url(r'^change-password/$',views.user_change_password,name='user_change_password'),
    url(r'^bookings/$',views.pickup_bookings,name='pickup_bookings'),
    url(r'^forgot-password/$',views.forgot_password,name='forgot_password'),
    url(r'^forgot-password/info/$',views.forgot_password_link_info,name='forgot_password_link_info'),
    url(r'^(?P<key>.+)/(?P<user_id>.+)/set-password/$',views.set_password,name='set_password'),
    url(r'^(?P<user_id>.+)/(?P<auth_token>.+)/pick-address-all/api/$',views.all_pickup_address_api,name='all_pickup_address_api'),
    url(r'^(?P<user_id>.+)/(?P<auth_token>.+)/drop-address-all/api/$',views.all_drop_address_api,name='all_drop_address_api'),
    url(r'^mobile-auth-api/$',views.mobile_auth_api,name='mobile_auth_api'),
    url(r'^mobile-auth-api-otp/$',views.mobile_auth_api_otp,name='mobile_auth_api_otp'),
    url(r'^(?P<user_id>.+)/mobile-auth-api-otp-resend/$',views.mobile_auth_api_otp_resend,name='mobile_auth_api_otp_resend'),
)