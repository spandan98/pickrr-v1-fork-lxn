from django.db import models
from django.core.validators import MinLengthValidator
from django.contrib.auth import get_user_model
from helpers.helpers import validate_pincode, validate_phone_number,generate_mobile_api_token

#Most appropriate way of getting user as it considers the active user model for your project. Either from auth,allauth or custom user model
User = get_user_model()

class MainUser(models.Model):
    #username and password will be user field
    user = models.OneToOneField(User,blank=True,null=True)
    name = models.CharField(max_length=128,verbose_name="Full Name",blank=True,null=True)
    #Form validation check: registered user with this email address already exists
    email= models.EmailField(verbose_name="Email Address")
    phone_number = models.CharField(validators=[validate_phone_number],max_length=14,verbose_name="Phone Number",blank=True,null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_guest = models.BooleanField(default=False)
    key = models.TextField(blank=True,null=True)
    mobile_token = models.TextField(blank=True,null=True)
    from_mobile = models.BooleanField(default=False)
    otp = models.CharField(max_length=5,blank=True,null=True)
    no_email = models.BooleanField(default=False,verbose_name='No email/sms')
    pickup_notification = models.BooleanField(default=True)
    avg_per_day_order = models.IntegerField(default=1)
    api_token = models.BooleanField(default=False)
    tracking_code = models.CharField(max_length=4,blank=True,null=True)
    is_premium = models.BooleanField(default=False)
    company_name = models.CharField(max_length=64,blank=True,null=True)

    def __unicode__(self):
        return self.email

    def save(self, *args, **kwargs):
        if self.api_token and self.pk is not None:
            if self.mobile_token:
                pass
            else:
                self.mobile_token = str(generate_mobile_api_token(self.pk))+ str(self.pk)
        super(MainUser, self).save(*args, **kwargs)

    class Meta:
        ordering = ['-date_joined']

class PickupAddress(models.Model):
    user = models.ForeignKey(MainUser,blank=True,null=True)
    name = models.CharField(max_length=128,verbose_name="Full Name")
    address_line = models.TextField()
    city = models.CharField(max_length=128,verbose_name="City / Town")
    state = models.CharField(max_length=128)
    pin_code = models.CharField(validators=[validate_pincode],max_length=6,verbose_name="Pincode")
    phone_number = models.CharField(validators=[validate_phone_number],max_length=14,verbose_name="Phone Number")

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-pk']
            
class DeliveryAddress(models.Model):
    user = models.ForeignKey(MainUser,blank=True,null=True)
    name = models.CharField(max_length=128,verbose_name="Full Name")
    address_line = models.TextField(verbose_name="Adress Details")
    city = models.CharField(max_length=128,verbose_name="City / Town")
    state = models.CharField(max_length=128)
    pin_code = models.CharField(validators=[validate_pincode],max_length=6,verbose_name="Pincode")
    phone_number = models.CharField(validators=[validate_phone_number],max_length=14,verbose_name="Phone Number")

    def __unicode__(self):
        return self.user.email

    class Meta:
        ordering = ['-pk']