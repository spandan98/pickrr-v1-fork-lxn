from django import forms
from django.forms import Textarea
from django.contrib.admin import widgets
from django.contrib.auth.hashers import check_password
from users.models import MainUser, PickupAddress, DeliveryAddress
from helpers.helpers import get_or_none
from locations.models import City, State

class SignupForm(forms.ModelForm):
    password = forms.CharField(label="Password",widget = forms.PasswordInput(attrs={'placeholder': 'Minimum length of Pasword must be 6'}))
    confirm_password = forms.CharField(label="Confirm Password",widget = forms.PasswordInput())

    class Meta:
        model = MainUser
        fields = ('name','email','phone_number')
        exclude = ('user','is_guest')
        widgets = {
        'password': forms.PasswordInput()
    }

    #Custom validation
    def clean(self):
        cleaned_data = super(SignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        email = cleaned_data.get("email")
        if email:
            email_exist = get_or_none(MainUser,email=email,is_guest=False)
            if email_exist:
                msg = u"This email ID is already registered.Either retrieve password or use different email ID"
                self._errors["email"] = self.error_class([msg])
                del cleaned_data["email"]
        if password and confirm_password:
            if len(password) < 6:
                msg = u"Pasword must be of length greater than or equal to 6"
                self._errors["password"] = self.error_class([msg])
                del cleaned_data["password"]
            if password != confirm_password:
                msg = u"Pasword did not match"
                self._errors["confirm_password"] = self.error_class([msg])
                del cleaned_data["confirm_password"]
        return cleaned_data

class ProfileEditForm(forms.ModelForm):
    class Meta:
        model=MainUser
        fields=('name','phone_number')
        exclude=('user','email','is_guest')
            

class LoginForm(forms.Form):
    email = forms.EmailField(label="Registered Email Address",
        widget=forms.TextInput(attrs={'id': 'login_email'}))
    password = forms.CharField(widget = forms.PasswordInput())

    #Custom validation for login form
    #Concept: https://docs.djangoproject.com/en/1.5/ref/forms/validation/#django.forms.Form.clean
    def clean(self):
        #super(LoginForm, self).clean() ensures that any validation logic in the parent classes is maintained
        cleaned_data = super(LoginForm, self).clean()
        email = cleaned_data.get("email")
        password = cleaned_data.get("password")
        if email and password:
            # Only do something if both fields are valid so far!
            user_obj = get_or_none(MainUser,email=email,is_guest=False)
            if not user_obj:
                #raise forms.ValidationError("Invalid Email Address")
                #Adding validation error in self._errors of email field
                msg = u"Invalid Email Address"
                self._errors["email"] = self.error_class([msg])
                # Email field is no longer valid.Thus, Remove it from the cleaned data.
                del cleaned_data["email"]
            else:
                user = user_obj.user
                if not check_password(password,user.password):
                    #raise forms.ValidationError("Password did not match")
                    #Adding validation error in self._errors of Password field
                    msg = u"Password did not match"
                    self._errors["password"] = self.error_class([msg])
                    # Password field is no longer valid.Thus, Remove it from the cleaned data.
                    del cleaned_data["password"]
                if not user.is_active:
                    msg = u"Your account has been deactivated.Please contact 09818197991 "
                    self._errors["email"] = self.error_class([msg])
                    # Password field is no longer valid.Thus, Remove it from the cleaned data.
                    del cleaned_data["email"]
        # return full collection of cleaned data.
        return cleaned_data

class PickupAddressForm(forms.ModelForm):
    from_city = forms.ChoiceField(label="City",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    #from_state = forms.ChoiceField(label="State",required=True,widget=forms.Select(attrs={'class': 'pickrr-drop'}))

    class Meta:
        model = PickupAddress
        fields = ('name','phone_number','address_line','pin_code')
        exclude = ('user','city','state')
        widgets = {
            'address_line': Textarea(attrs={'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(PickupAddressForm, self).__init__(*args, **kwargs)
        #For dynamically changing the from city values as per our expansion plans
        self.fields['from_city'].choices = [ (from_city.id, str(from_city.name)) for from_city in City.objects.filter(from_service=True)]
        #self.fields['from_state'].choices = list(set([ (from_city.state.id, str(from_city.state.name)) for from_city in City.objects.filter(from_service=True)]))
        self.fields.keyOrder = [
            'name',
            'phone_number',
            'address_line',
            'from_city',
            'pin_code']
    #TODO: validation of pincode as per selected inputs

class DeliveryAddressForm(forms.ModelForm):
    to_city = forms.ChoiceField(label="City",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    #to_state = forms.ChoiceField(label="State",required=True,widget=forms.Select(attrs={'class': 'pickrr-drop'}))

    class Meta:
        model = DeliveryAddress
        fields = ('name','phone_number','address_line','pin_code')
        exclude = ('user','city','state')
        widgets = {
            'address_line': Textarea(attrs={'rows': 5}),
        }

    def __init__(self, *args, **kwargs):
        super(DeliveryAddressForm, self).__init__(*args, **kwargs)
        #For dynamically changing the from city values as per our expansion plans
        self.fields['to_city'].choices = [ (city.id, str(city.name)) for city in City.objects.all()]
        #self.fields['to_state'].choices = [ (state.id, str(state.name)) for state in State.objects.all()]
        self.fields.keyOrder = [
            'name',
            'phone_number',
            'address_line',
            'to_city',
            'pin_code']


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(label="Old Password",widget = forms.PasswordInput())
    new_password = forms.CharField(label="New Password",widget = forms.PasswordInput())
    confirm_password = forms.CharField(label = "Confirm Password",widget = forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(ChangePasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        user = self.request.user
        cleaned_data = super(ChangePasswordForm, self).clean()
        old_password = cleaned_data.get("old_password")
        new_password = cleaned_data.get("new_password")
        confirm_password = cleaned_data.get("confirm_password")
        if old_password and new_password and confirm_password:
            if len(new_password) < 6:
                msg = u"Pasword must be of length greater than or equal to 6"
                self._errors["new_password"] = self.error_class([msg])
                del cleaned_data["new_password"]
            if new_password != confirm_password:
                msg = u"Pasword did not match"
                self._errors["confirm_password"] = self.error_class([msg])
                del cleaned_data["confirm_password"]
            if len(old_password) < 6:
                msg = u"Password did not match with old password"
                self._errors["old_password"] = self.error_class([msg])
                del cleaned_data["old_password"]
            #After custom validation if all three exist then do the database matching
            if old_password and new_password and confirm_password:
                if not check_password(old_password,user.password):
                    msg = u"Password did not match"
                    self._errors["old_password"] = self.error_class([msg])
                    del cleaned_data["old_password"] 
                else:
                    #set password here itself
                    user.set_password(new_password)
                    user.save()
        return cleaned_data


class ForgotPasswordForm(forms.Form):
    email = forms.EmailField(label="Registered Email Address")

    #Custom validation for Forgot Password form
    def clean(self):
        #super(ForgotPasswordForm, self).clean() ensures that any validation logic in the parent classes is maintained
        cleaned_data = super(ForgotPasswordForm, self).clean()
        email = cleaned_data.get("email")
        if email:
            user_obj = get_or_none(MainUser,email=email,is_guest=False)
            if not user_obj:
                #Adding validation error in self._errors of email field
                msg = u"Invalid Email Address. You need to Signup! :)"
                self._errors["email"] = self.error_class([msg])
                # Email field is no longer valid.Thus, Remove it from the cleaned data.
                del cleaned_data["email"]
        return cleaned_data

class SetPasswordForm(forms.Form):
    new_password = forms.CharField(label="New Password",widget = forms.PasswordInput())
    confirm_password = forms.CharField(label = "Confirm Password",widget = forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.user_id = kwargs.pop("user_id")
        super(SetPasswordForm, self).__init__(*args, **kwargs)

    def clean(self):
        user_id = self.user_id
        cleaned_data = super(SetPasswordForm, self).clean()
        new_password = cleaned_data.get("new_password")
        confirm_password = cleaned_data.get("confirm_password")
        if new_password and confirm_password:
            if len(new_password) < 6:
                msg = u"Pasword must be of length greater than or equal to 6"
                self._errors["new_password"] = self.error_class([msg])
                del cleaned_data["new_password"]
            if new_password != confirm_password:
                msg = u"Pasword did not match"
                self._errors["confirm_password"] = self.error_class([msg])
                del cleaned_data["confirm_password"]
            #After custom validation if all three exist then do the database matching
            if cleaned_data.get("new_password") and cleaned_data.get("confirm_password"):
                #set password here itself
                user_obj = get_or_none(MainUser,pk=user_id)
                if user_obj:
                    user = user_obj.user
                    user.set_password(new_password)
                    user.save()
        return cleaned_data