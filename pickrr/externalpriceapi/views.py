from django.shortcuts import render_to_response,redirect, get_object_or_404, Http404
from django.template import RequestContext
from externalpriceapi.forms import PriceApiForm
from locations.models import *
from pricingmodel.models import *
import math
#from rest_framework.authentication import TokenAuthentication
#from rest_framework.permissions import IsAuthenticated
from helpers.helpers import JsonResponse
#from django_remote_forms.forms import RemoteForm

def service_price(from_location_id,to_location_id,weight):
    from_city = get_object_or_404(City,pk=from_location_id)
    to_city = get_object_or_404(City,pk=to_location_id)
    service_dict = {}
    #Define Intracity:
    if to_city.from_service and to_city.to_service:
        zone = get_object_or_404(PickrrZone,zone_name='Intracity')
        services = zone.pickrrservices_set.all()
        for service in services:
            volume = int(math.ceil(float(weight)/float(service.base_weight)))
            charge = service.base_price + service.add_price*(volume-1)
            service_dict[str(service.service_name)] = charge
        service_dict['type'] = 'Intracity'
    #Intercity
    else:
        zone = to_city.pickrr_zone
        services = zone.pickrrservices_set.all()
        for service in services:
            volume = int(math.ceil(float(weight)/float(service.base_weight)))
            charge = service.base_price + service.add_price*(volume-1)
            service_dict[str(service.service_name)] = charge
        service_dict['type'] = 'Intercity'
    return service_dict

#This function takes lbh and weight as input and outputs the price of three services offered by us
def calculate_price(request):
    priceform = PriceApiForm()
    if request.method=='POST':
        priceform = PriceApiForm(request.POST)
        if priceform.is_valid():
            from_location = priceform.cleaned_data['from_location']
            #from_location = dict(priceform.fields['from_location'].choices)[int(from_location)]
            to_location = priceform.cleaned_data['to_location']
            #to_location = dict(priceform.fields['to_location'].choices)[int(to_location)]
            length = priceform.cleaned_data['length']
            breadth = priceform.cleaned_data['breadth']
            height = priceform.cleaned_data['height']
            weight = priceform.cleaned_data['weight']
            weight_lbh = length*breadth*height
            #weight_lbh in kg
            weight_lbh = weight_lbh/float(5000)
            valid_weight = weight
            if weight_lbh > weight:
                valid_weight = weight_lbh
            service_dict = service_price(int(from_location),
                                        int(to_location),
                                        valid_weight)
            rush_price = service_dict['Rush']
            premium_price = service_dict['Premium']
            standard_price = service_dict['Standard']
            service_type = service_dict['type']
            return render_to_response('externalpriceapi/show_price.html',
                                {'rush_price':rush_price,
                                'premium_price':premium_price,
                                'standard_price':standard_price,
                                'service_type':service_type,
                                'priceform':priceform
                                },
                                context_instance=RequestContext(request)
                            )
    return render_to_response('externalpriceapi/_calculate_price.html',
                                {'priceform':priceform},
                                context_instance=RequestContext(request))

def show_price(request,rush_price,premium_price,standard_price,service_type):
    priceform = PriceApiForm(request.POST)
    return render_to_response('externalpriceapi/show_price.html',
                                {'rush_price':rush_price,
                                'premium_price':premium_price,
                                'standard_price':standard_price,
                                'service_type':service_type,
                                'priceform':priceform
                                }
                                ,context_instance=RequestContext(request))

#Price calculator api for mobile app
def price_cal_API(request,from_location_id,to_location_id,length,
                    breadth,height,weight):
    weight_lbh = float(length)*float(breadth)*float(height)
    #weight_lbh in kg
    weight_lbh = weight_lbh/float(5000)
    valid_weight = float(weight)
    if weight_lbh > weight:
        valid_weight = weight_lbh
    #print "hello"
    service_dict = service_price(int(from_location_id),
                                int(to_location_id),
                                valid_weight)
    rush_price = service_dict['Rush']
    premium_price = service_dict['Premium']
    standard_price = service_dict['Standard']
    service_type = service_dict['type']
    #priceform = PriceApiForm()
    #remote_form = RemoteForm(priceform)
    #remote_form_dict = remote_form.as_dict()
    response_data = {}
    if service_type == 'Intercity':
        rush_timeline = 'within 1-2 days'
        premium_timeline = 'within 2-4 days'
        standard_timeline = 'within 4-6 days'
    else:
        rush_timeline = 'within 90 min'
        premium_timeline = 'within 4-5 hrs'
        standard_timeline = 'within 24 hrs'
    #response_data['service_type'] = service_type
    response_data['rush_price'] = rush_price
    response_data['rush_timeline'] = rush_timeline
    response_data['premium_price'] = premium_price
    response_data['premium_timeline'] = premium_timeline
    response_data['standard_price'] = standard_price
    response_data['standard_timeline'] = standard_timeline
    #response_data['priceform'] = remote_form_dict
    return JsonResponse(response_data)