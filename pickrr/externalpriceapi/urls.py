from django.conf.urls import patterns, url
from externalpriceapi import views

urlpatterns = patterns('',
    url(r'^$',views.calculate_price,name='calculate_price'),
    url(r'^(?P<rush_price>.+)/(?P<premium_price>.+)/(?P<standard_price>.+)/(?P<service_type>.+)/show-price/$',views.show_price,name='show_price'),
    url(r'^(?P<from_location_id>.+)/(?P<to_location_id>.+)/(?P<length>.+)/(?P<breadth>.+)/(?P<height>.+)/(?P<weight>.+)/price-cal-api/$',views.price_cal_API,name='price_cal_API'),
)