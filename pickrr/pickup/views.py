from django.shortcuts import render_to_response, get_object_or_404, redirect, get_list_or_404, Http404
from django.core.urlresolvers import reverse
from django.template import Context, RequestContext
from django.core.context_processors import csrf
from pricingmodel.models import PickrrZone
from products.models import Product
from locations.models import *
from pickup.forms import SchedulePickupForm
from users.models import MainUser
from helpers.helpers import JsonResponse

def get_value_from_request(get_array,request):
    value_array = []
    for value in get_array:
        get_id = request.session.get(value,False)
        if not get_id:
            raise Http404
        else:
            #del request.session[value]
            value_array.append(get_id)
    return value_array

#schedule pickup checkout flow starts from here
def home_pickrr(request):
    """
    #SCript for adding archies data to their account
    from track.models import Status
    user_obj = get_object_or_404(MainUser,email='archies@pickrr.com')
    track_arch = get_list_or_404(Status,tracking_id__icontains='PICK')
    for track in track_arch:
        track.order.user = user_obj
        track.order.save()
    from track.models import Status
    track_list = Status.objects.all()
    from users.views import key_gen
    for track in track_list:
        if not track.key:
            track.key=key_gen(track.pk)
            track.save()
    from helpers.helpers import generate_mobile_api_token, filter_or_none
    user_list = filter_or_none(MainUser,is_guest=False)
    print user_list
    for user_obj in user_list:
        if not user_obj.mobile_token:
            auth_token = str(generate_mobile_api_token(user_obj.pk))+ str(user_obj.pk)
            user_obj.mobile_token = auth_token
            user_obj.save()
    """
    pickupform = SchedulePickupForm()
    if request.method=='POST':
        pickupform = SchedulePickupForm(request.POST)
        if pickupform.is_valid():
            from_location_id = pickupform.cleaned_data['from_location']
            to_location_id = pickupform.cleaned_data['to_location']
            #product_id = pickupform.cleaned_data['product']
            request.session['from_location_id'] = from_location_id
            request.session['to_location_id'] = to_location_id
            #request.session['product_id'] = product_id
            return redirect('pickup:select_service')
    return render_to_response('pickup/home_pickrr.html',
                            {'pickupform':pickupform},
                            context_instance=RequestContext(request))

def select_service(request):
    if request.method=='POST':
        service_id = request.POST.get('service_type')
        if not service_id:
            raise Http404
        #print request.session.get('from_location_id',False)
        request.session['service_id'] = service_id
        if request.user.is_authenticated():
            user = get_object_or_404(MainUser,user=request.user)
            request.session['user_id'] = user.pk
            return redirect('orders:checkout_pickup_address')
        else:
            return redirect('orders:checkout_flow')
    request_parameter_array = ['from_location_id','to_location_id']
    id_array = get_value_from_request(request_parameter_array,request)
    from_city = get_object_or_404(City,pk=id_array[0])
    to_city = get_object_or_404(City,pk=id_array[1])
    #product = get_object_or_404(Product,pk=id_array[2])
    #Define Intracity:
    if to_city.from_service and to_city.to_service:
        service_type = 'Intracity'
        zone = get_object_or_404(PickrrZone,zone_name='Intracity')
        services = zone.pickrrservices_set.all()
    #Intercity
    else:
        service_type = 'Intercity'
        zone = to_city.pickrr_zone
        services = zone.pickrrservices_set.all()
    return render_to_response('pickup/select_service.html',
                            {'from_city':from_city,
                            'to_city':to_city,
                            #'product':product,
                            'services':services,
                            'service_type':service_type},
                            context_instance=RequestContext(request))

def show_service_api(request,from_location_id,to_location_id):
    from_city = get_object_or_404(City,pk=int(from_location_id))
    to_city = get_object_or_404(City,pk=int(to_location_id))
    #Define Intracity:
    if to_city.from_service and to_city.to_service:
        service_type = 'Intracity'
        rush_timeline = 'within 90 min'
        rush_base_fare = 150
        rush_base_weight = 500
        rush_add_fare = 100
        premium_timeline = 'within 4-5 hrs'
        premium_base_fare = 80
        premium_base_weight = 500
        premium_add_fare = 80
        standard_timeline = 'within 24 hrs'
        standard_base_fare = 59
        standard_base_weight = 500
        standard_add_fare = 59
    #Intercity
    else:
        service_type = 'Intercity'
        zone = to_city.pickrr_zone
        services = zone.pickrrservices_set.all()
        for service in services:
            if service.service_name == 'Rush':
                rush_base_fare = service.base_price
                rush_add_fare = service.add_price
            elif service.service_name == 'Premium':
                premium_base_fare = service.base_price
                premium_add_fare = service.add_price
            else:
                standard_base_fare = service.base_price
                standard_add_fare = service.add_price
        rush_timeline = 'within 1-2 days'
        #rush_base_fare = 180
        rush_base_weight = 500
        #rush_add_fare = 120
        premium_timeline = 'within 2-4 days'
        #premium_base_fare = 120
        premium_base_weight = 500
        #premium_add_fare = 60
        standard_timeline = 'within 4-6 days'
        #standard_base_fare = 65
        standard_base_weight = 500
        #standard_add_fare = 60
    response_data = {}
    response_data['rush_timeline'] = rush_timeline
    response_data['rush_base_fare'] = rush_base_fare
    response_data['rush_base_weight'] = rush_base_weight
    response_data['rush_add_fare'] = rush_add_fare
    response_data['premium_timeline'] = premium_timeline
    response_data['premium_base_fare'] = premium_base_fare
    response_data['premium_base_weight'] = premium_base_weight
    response_data['premium_add_fare'] = premium_add_fare
    response_data['standard_timeline'] = standard_timeline
    response_data['standard_base_fare'] = standard_base_fare
    response_data['standard_base_weight'] = standard_base_weight
    response_data['standard_add_fare'] = standard_add_fare
    return JsonResponse(response_data)