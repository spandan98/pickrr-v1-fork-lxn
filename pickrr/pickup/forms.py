from django import forms
from locations.models import City
from products.models import Product
import random

class SchedulePickupForm(forms.Form):
    from_location = forms.ChoiceField(label="From Location",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    to_location = forms.ChoiceField(label="To Location",required=True,
        widget=forms.Select(attrs={'class': 'pickrr-drop'}))
    #product = forms.ChoiceField(label="Enter items to be shipped",required=True)

    def __init__(self, *args, **kwargs):
        super(SchedulePickupForm, self).__init__(*args, **kwargs)
        #For dynamically changing the from city values as per our expansion plans
        self.fields['from_location'].choices = [ (from_city.id, str(from_city.name)) for from_city in sorted(City.objects.filter(from_service=True),key=lambda x: random.random())]
        #For dynamically changing the to city
        self.fields['to_location'].choices = [ (to_city.id, str(to_city.name)) for to_city in sorted(City.objects.filter(to_service=True),key=lambda x: random.random())]
        #self.fields['product'].choices = [ (prod.id, str(prod.name)) for prod in Product.objects.all()]