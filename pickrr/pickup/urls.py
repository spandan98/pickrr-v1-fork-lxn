from django.conf.urls import patterns, url
from pickup import views

urlpatterns = patterns('',
    url(r'^$',views.home_pickrr,name='home_pickrr'),
    url(r'^select-service/$',views.select_service,name='select_service'),
    url(r'^(?P<from_location_id>.+)/(?P<to_location_id>.+)/show-service-api/$',views.show_service_api,name='show_service_api'),
)