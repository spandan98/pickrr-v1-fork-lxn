from users.models import MainUser, PickupAddress, DeliveryAddress
from locations.models import City, Pincode
from helpers.helpers import JsonResponse, read_post_json, get_or_none, generate_otp, send_sms, generate_mobile_api_token, clean_phone_number, random_password, JsonQueryset, correction_in_time
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.contrib.auth import authenticate, login, logout
from orders.models import Order, OrderItems
from products.models import Product
from track.models import Status
from django.contrib.auth.hashers import check_password
from pricingmodel.models import PickrrZone, PickrrServices
from pickrrs.models import Pickrrs

User = get_user_model()

"""
Input:
name
phone_number
email
password: if not password then mobile user, hence send otp, else if password, web user, send success message

Output:
auth_token <string>
err: None if success, <string>
Notes: if mobile: then send otp and use OTP auth api then. Further, send an email with auto-generated password
"""
@csrf_exempt
def pickrr_registration_api(request):
    response_data = {}
    if request.method=='POST':
        register_data = read_post_json(request.body)
        name = register_data['name']
        phone_number = register_data['phone_number']
        email = register_data['email']
        user_obj,is_created = MainUser.objects.get_or_create(email=str(email),is_guest=False)
        if not is_created:
            response_data['err'] = 'User with this email ID is already registered'
            return JsonResponse(response_data)
        user_obj.name = str(name)
        user_obj.phone_number = str(phone_number)
        #web-user
        if 'password' in register_data:
            username = 'mainuser' + str(user_obj.pk)
            #creating a user object corresponding to the customer user
            user = User.objects.create_user(username,user_obj.email,str(register_data['password']))
            user.is_active = True
            user.save()
            user_obj.user = user
            auth_token = str(generate_mobile_api_token(user_obj.pk))+ str(user_obj.pk)
            user_obj.mobile_token = auth_token
            user_obj.save()
            response_data['auth_token'] = auth_token
            response_data['err'] = None
            return JsonResponse(response_data)
        #mobile-user
        else:
            username = 'mainuser' + str(user_obj.pk)
            #creating a user object corresponding to the customer user
            password = random_password(7)
            user = User.objects.create_user(username,user_obj.email,password)
            user.is_active = False
            user.save()
            user_obj.user = user
            user_obj.otp = generate_otp(4)
            auth_token = str(generate_mobile_api_token(user_obj.pk))+ str(user_obj.pk)
            user_obj.mobile_token = auth_token
            user_obj.save()
            destination_number = clean_phone_number(str(phone_number))
            first_line = 'Dear customer' + '\n' 
            second_line = ('Your OTP for verification of mobile number',
                        str(phone_number),
                        'is',
                        str(user_obj.otp))
            second_line = ' '.join(second_line)
            second_line = second_line + '.'
            message = first_line + second_line
            send_sms(destination_number,message)
            #send email of web login details
            subject = "Welcome to Picrr! Web login details for " + str(user_obj.email)
            body = render_to_string('users/welcome_mobile_to_web.html', {'user_obj':user_obj,'password':password})
            email = EmailMessage(subject, body, to=[user_obj.email])
            email.content_subtype = "html"
            email.send()
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
OTP Auth API

Input:
phone_number
email
otp

Output:
auth_token
err: None if success, <string>

"""
@csrf_exempt
def pickrr_otp_auth_api(request):
    response_data = {}
    if request.method=='POST':
        otp_data = read_post_json(request.body)
        phone_number = str(otp_data['phone_number'])
        email = str(otp_data['email'])
        otp = str(otp_data['otp'])
        user_obj = get_or_none(MainUser,email=email,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user email ID'
            return JsonResponse(response_data)
        if str(user_obj.otp) != otp:
            response_data['err'] = 'Invalid OTP.Please enter a valid one'
            return JsonResponse(response_data)
        else:
            user_obj.user.is_active = True
            user_obj.user.save()
            response_data['auth_token'] = user_obj.mobile_token
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Resend OTP API

input:
phone_number
email

Output:
otp on mobile 
err: None if success, <string>
"""
@csrf_exempt
def pickrr_otp_resend_api(request):
    response_data = {}
    if request.method=='POST':
        otp_data = read_post_json(request.body)
        phone_number = str(otp_data['phone_number'])
        email = str(otp_data['email'])
        user_obj = get_or_none(MainUser,email=email,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user email ID'
            return JsonResponse(response_data)
        destination_number = clean_phone_number(phone_number)
        first_line = 'Dear customer' + '\n' 
        second_line = ('Your OTP for verification of mobile number',
                        str(phone_number),
                        'is',
                        str(user_obj.otp))
        second_line = ' '.join(second_line)
        second_line = second_line + '.'
        message = first_line + second_line
        send_sms(destination_number,message)
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)


"""
Web Login API

Input:
email
password

Output:
auth_token
err: None if success, <string>
"""
@csrf_exempt
def pickrr_web_login_api(request):
    response_data = {}
    if request.method=='POST':
        login_data = read_post_json(request.body)
        email = str(login_data['email'])
        password = str(login_data['password'])
        user_obj = get_or_none(MainUser,email=email,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user email ID'
            return JsonResponse(response_data)
        user = authenticate(username=user_obj.user.username, password=password)
        if user is not None:
            if user.is_active:
                login(request,user)
                response_data['auth_token'] = user_obj.mobile_token
                response_data['err'] = None
                return JsonResponse(response_data)
            else:
                response_data['err'] = 'Your account has been deactivated. Please contact 09818197991'
                return JsonResponse(response_data)
                #return HttpResponse('Your account has been deactivated. Please contact 09818197991')
        else:
            #invalid email or password
            response_data['err'] = 'Password did not match. Try again'
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
App Login API

Input:
phone_number
email

Output:
otp through SMS
err: None if success, <string>
"""
@csrf_exempt
def pickrr_app_login_api(request):
    response_data = {}
    if request.method=='POST':
        login_data = read_post_json(request.body)
        phone_number = str(login_data['phone_number'])
        email = str(login_data['email'])
        user_obj = get_or_none(MainUser,email=email,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user email ID'
            return JsonResponse(response_data)
        destination_number = clean_phone_number(phone_number)
        first_line = 'Dear customer' + '\n' 
        second_line = ('Your OTP for verification of mobile number',
                        str(phone_number),
                        'is',
                        str(user_obj.otp))
        second_line = ' '.join(second_line)
        second_line = second_line + '.'
        message = first_line + second_line
        send_sms(destination_number,message)
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Pickrr Service Pricing API:
Input:
from_pincode*
to_pincode*
current_time : If current_time < 1600 hrs, 
then same day delivery not possible. Furthermore computations will happen on current time in future.

Output:
rush_timeline<string>
rush_unit_price <float>
rush_additional_price <float>
premium_timeline<string>
premium_unit_price <float>
premium_additional_price <float>
standard_timeline<string>
standard_unit_price <float>
standard_additional_price <float>
err: None if success, <string>
"""
#TODO: Current Time Factor for disabling Rush Pickups
@csrf_exempt
def pickrr_service_pricing_api(request):
    response_data = {}
    if request.method=='POST':
        location_data = read_post_json(request.body)
        from_pincode = location_data['from_pincode']
        to_pincode = location_data['to_pincode']
        from_pincode_obj = get_or_none(Pincode,pincode=str(from_pincode))
        if not from_pincode_obj:
            response_data['err'] = 'Sincere Apologies as currently our pickup service is supported only for Gurgaon, New Delhi, Noida and Faridabad'
            return JsonResponse(response_data)
        if not from_pincode_obj.region.city.from_service:
            response_data['err'] = 'Sincere Apologies as currently our pickup service is supported only for Gurgaon, New Delhi, Noida and Faridabad'
            return JsonResponse(response_data)
        to_pincode_obj = get_or_none(Pincode,pincode=str(to_pincode))
        """
        JUGAAD: our pincode list is not exhaustive. 
        Hence, any missing pincode will be treated as inter-city order
        """
        if not to_pincode_obj:
            response_data['rush_timeline'] = 'within 1-2 days'
            response_data['rush_unit_price'] = float(180)
            response_data['rush_additional_price'] = float(120)
            response_data['premium_timeline'] = 'within 2-4 days'
            response_data['premium_unit_price'] = float(120)
            response_data['premium_additional_price'] = float(60)
            response_data['standard_timeline'] = 'within 4-6 days'
            response_data['standard_unit_price'] = float(65)
            response_data['standard_additional_price'] = float(60)
            response_data['err'] = None
            return JsonResponse(response_data)
        #both from and to pincodes are courier supported
        from_city = from_pincode_obj.region.city
        to_city = to_pincode_obj.region.city
        #intra-city parameters
        if to_city.from_service:
            #rush_service possibility only in same city for now
            if from_city.pk == to_city.pk:
                response_data['rush_timeline'] = 'Within 90 mins'
                response_data['rush_unit_price'] = float(150)
                response_data['rush_additional_price'] = float(100)
                response_data['premium_timeline'] = 'within 4-5 hrs'
                response_data['premium_unit_price'] = float(80)
                response_data['premium_additional_price'] = float(80)
                response_data['standard_timeline'] = 'within 24 hrs'
                response_data['standard_unit_price'] = float(59)
                response_data['standard_additional_price'] = float(59)
                response_data['err'] = None
                return JsonResponse(response_data)
                #TODO: all intra-city services except for time restrictions
            else:
                #only premium and standard services are available
                response_data['premium_timeline'] = 'within 4-5 hrs'
                response_data['premium_unit_price'] = float(80)
                response_data['premium_additional_price'] = float(80)
                response_data['standard_timeline'] = 'within 24 hrs'
                response_data['standard_unit_price'] = float(59)
                response_data['standard_additional_price'] = float(59)
                response_data['err'] = None
                return JsonResponse(response_data)
        #inter-city parameters
        else:
            zone = to_city.pickrr_zone
            services = zone.pickrrservices_set.all()
            for service in services:
                if str(service.service_name) == 'Rush':
                    response_data['rush_timeline'] = 'within 1-2 days'
                    response_data['rush_unit_price'] = service.base_price
                    response_data['rush_additional_price'] = service.add_price
                elif str(service.service_name) == 'Premium':
                    response_data['premium_timeline'] = 'within 2-4 days'
                    response_data['premium_unit_price'] = service.base_price
                    response_data['premium_additional_price'] = service.add_price
                else:
                    response_data['standard_timeline'] = 'within 4-6 days'
                    response_data['standard_unit_price'] = service.base_price
                    response_data['standard_additional_price'] = service.add_price
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Order Tracking API

Input: 
Tracking_id in URL

Output:
order_status
pickup_status if exists
warehouse_status if exists
transit_status if exists
delivery_status if exists
final_status if exists
err: None if success, <string>
"""
def pickrr_tracking_api(request,tracking_id):
    response_data = {}
    track_obj = get_or_none(Status,tracking_id=str(tracking_id))
    if not track_obj:
        response_data['err'] = 'Invalid Tracking ID'
        return JsonResponse(response_data)
    response_data['order_status_date'] = correction_in_time(track_obj.order.placed_at).split(',')[0]
    response_data['order_status_time'] = correction_in_time(track_obj.order.placed_at).split(',')[1]
    #response_data['order_status'] = 'Placed on ' + correction_in_time(track_obj.order.placed_at)
    if track_obj.pickup_time:
        response_data['pickup_status_date'] = correction_in_time(track_obj.pickup_time).split(',')[0]
        response_data['pickup_status_time'] = correction_in_time(track_obj.pickup_time).split(',')[1]
        #response_data['pickup_status'] = 'Picked on ' + correction_in_time(track_obj.pickup_time)
    if track_obj.warehouse_receive_time:
        response_data['warehouse_status_date'] = correction_in_time(track_obj.warehouse_receive_time).split(',')[0]
        response_data['warehouse_status_time'] = correction_in_time(track_obj.warehouse_receive_time).split(',')[1]
        #response_data['warehouse_status'] = 'Reached Pickrr Warehouse on ' + correction_in_time(track_obj.warehouse_receive_time)
    if track_obj.in_transit_time:
        response_data['transit_status_date'] = correction_in_time(track_obj.in_transit_time).split(',')[0]
        response_data['transit_status_time'] = correction_in_time(track_obj.in_transit_time).split(',')[1]
        #response_data['transit_status'] = str(track_obj.in_transit_status) + ' from ' + correction_in_time(track_obj.in_transit_time)
    if track_obj.out_delivery_time:
        response_data['delivery_status_date'] = correction_in_time(track_obj.out_delivery_time).split(',')[0]
        response_data['delivery_status_time'] = correction_in_time(track_obj.out_delivery_time).split(',')[1]
        #response_data['delivery_status'] = 'Out for Delivery from ' + correction_in_time(track_obj.out_delivery_time)
    if track_obj.delivery_date:
        response_data['final_status_date'] = correction_in_time(track_obj.delivery_date).split(',')[0]
        response_data['final_status_time'] = correction_in_time(track_obj.delivery_date).split(',')[1]
        #response_data['final_status'] = 'Successfully Delivered on ' + correction_in_time(track_obj.delivery_date)
    response_data['err'] = None
    return JsonResponse(response_data)

"""
Order History API

Input:
auth_token

Output:
array
err: None if success, <string>
"""
#API returns all orders of a particular user
@csrf_exempt
def pickrr_user_order_history_api(request):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        auth_token = user_data['auth_token']
        user_obj = get_or_none(MainUser,mobile_token=str(auth_token),is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user authentication token'
            return JsonResponse(response_data)
        if 'order_id' in user_data:
            response_list = []
            order = get_or_none(Order,order_id=str(user_data['order_id']))
            if not order:
                response_data['err'] = 'Invalid Order ID'
                return JsonResponse(response_data)
            track_obj = get_or_none(Status,order=order)
            if not track_obj:
                tracking_id = ''
            else:
                tracking_id = track_obj.tracking_id
            if order.invoice_sent:
                bill = 'Rs. ' + str(order.total_bill)
            else:
                bill = 'In Process'
            items = order.orderitems_set.all()
            if items.count():
                item_product = items[0]
                item_name = str(item_product.product.name)
            else:
                item_name = 'Parcel'
            if order.service_used:
                service_used = order.service_used.service_name
            else:
                service_used = 'Premium'
            if order.is_cancelled:
                is_cancelled = True
            else:
                is_cancelled = False
            sender_name = order.pickup_address.name
            sender_phone_number = order.pickup_address.phone_number
            pickup_address_line = order.pickup_address.address_line
            pickup_pincode = order.pickup_address.pin_code
            receiver_name = order.drop_address.name
            receiver_phone_number = order.drop_address.phone_number
            delivery_address_line = order.drop_address.address_line
            delivery_pincode = order.drop_address.pin_code
            order_dict = {
                'order_id' : order.order_id,
                'tracking_id' : tracking_id,
                'from_city' : order.pickup_address.city,
                'to_city' : order.drop_address.city,
                'placed_on' : order.placed_at.strftime("%B %d, %y, %H:%M"),
                'bill' : bill,
                'item_name': item_name,
                'service_used': service_used,
                'is_cancelled' : is_cancelled,
                'sender_name': sender_name,
                'sender_phone_number':sender_phone_number,
                'pickup_address_line':pickup_address_line,
                'pickup_pincode':pickup_pincode,
                'receiver_name':receiver_name,
                'receiver_phone_number':receiver_phone_number,
                'delivery_address_line':delivery_address_line,
                'delivery_pincode':delivery_pincode,
            }
            response_list.append(order_dict)
            response_data['order_list'] = response_list
            response_data['err'] = None
            return JsonResponse(response_data)
        if 'tracking_id' in user_data:
            response_list = []
            track_obj = get_or_none(Status,tracking_id=str(user_data['tracking_id']))
            if not track_obj:
                response_data['err'] = 'Invalid Tracking ID'
                return JsonResponse(response_data)
            order = track_obj.order
            if order.invoice_sent:
                bill = 'Rs. ' + str(order.total_bill)
            else:
                bill = 'In Process'
            items = order.orderitems_set.all()
            if items.count():
                item_product = items[0]
                item_name = str(item_product.product.name)
            else:
                item_name = 'Parcel'
            if order.service_used:
                service_used = order.service_used.service_name
            else:
                service_used = 'Premium'
            if order.is_cancelled:
                is_cancelled = True
            else:
                is_cancelled = False
            sender_name = order.pickup_address.name
            sender_phone_number = order.pickup_address.phone_number
            pickup_address_line = order.pickup_address.address_line
            pickup_pincode = order.pickup_address.pin_code
            receiver_name = order.drop_address.name
            receiver_phone_number = order.drop_address.phone_number
            delivery_address_line = order.drop_address.address_line
            delivery_pincode = order.drop_address.pin_code
            order_dict = {
                'order_id' : order.order_id,
                'tracking_id' : str(user_data['tracking_id']),
                'from_city' : order.pickup_address.city,
                'to_city' : order.drop_address.city,
                'placed_on' : order.placed_at.strftime("%B %d, %y, %H:%M"),
                'bill' : bill,
                'item_name': item_name,
                'service_used': service_used,
                'is_cancelled' : is_cancelled,
                'sender_name': sender_name,
                'sender_phone_number':sender_phone_number,
                'pickup_address_line':pickup_address_line,
                'pickup_pincode':pickup_pincode,
                'receiver_name':receiver_name,
                'receiver_phone_number':receiver_phone_number,
                'delivery_address_line':delivery_address_line,
                'delivery_pincode':delivery_pincode,
            }
            response_list.append(order_dict)
            response_data['order_list'] = response_list
            response_data['err'] = None
            return JsonResponse(response_data)
        order_list = user_obj.order_set.all()
        if 'page_number' in user_data:
            page_number = int(user_data['page_number'])
            if page_number == 0:
                response_data['err'] = 'Pagination start from page number 1. Please send 1 and above'
                return JsonResponse(response_data)
            else:
                order_list = order_list[(page_number-1)*15:(page_number*15)]
        #print len(order_list)
        response_list = []
        for order in order_list:
            track_obj = get_or_none(Status,order=order)
            if not track_obj:
                tracking_id = ''
            else:
                tracking_id = track_obj.tracking_id
            if order.invoice_sent:
                bill = 'Rs. ' + str(order.total_bill)
            else:
                bill = 'In Process'
            #TODO: item name tech flow
            #item_name = 'book'
            items = order.orderitems_set.all()
            if items.count():
                item_product = items[0]
                item_name = str(item_product.product.name)
            else:
                item_name = 'Parcel'
            if order.service_used:
                service_used = order.service_used.service_name
            else:
                service_used = 'Premium'
            if order.is_cancelled:
                is_cancelled = True
            else:
                is_cancelled = False
            sender_name = order.pickup_address.name
            sender_phone_number = order.pickup_address.phone_number
            pickup_address_line = order.pickup_address.address_line
            pickup_pincode = order.pickup_address.pin_code
            receiver_name = order.drop_address.name
            receiver_phone_number = order.drop_address.phone_number
            delivery_address_line = order.drop_address.address_line
            delivery_pincode = order.drop_address.pin_code
            order_dict = {
                'order_id' : order.order_id,
                'tracking_id' : tracking_id,
                'from_city' : order.pickup_address.city,
                'to_city' : order.drop_address.city,
                'placed_on' : order.placed_at.strftime("%B %d, %y, %H:%M"),
                'bill' : bill,
                'item_name': item_name,
                'service_used': service_used,
                'is_cancelled' : is_cancelled,
                'sender_name': sender_name,
                'sender_phone_number':sender_phone_number,
                'pickup_address_line':pickup_address_line,
                'pickup_pincode':pickup_pincode,
                'receiver_name':receiver_name,
                'receiver_phone_number':receiver_phone_number,
                'delivery_address_line':delivery_address_line,
                'delivery_pincode':delivery_pincode,
            }
            response_list.append(order_dict)
        response_data['order_list'] = response_list
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Edit User Info API

input:
auth_token
user_name
user_email
user_phone_number

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_edit_user_info_api(request):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        auth_token = user_data['auth_token']
        user_obj = get_or_none(MainUser,mobile_token=str(auth_token),is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user authentication token'
            return JsonResponse(response_data)
        if 'name' in user_data:
            user_obj.name = str(user_data['name'])
        if 'email' in user_data:
            user_obj.email = str(user_data['email'])
        if 'phone_number' in user_data:
            user_obj.phone_number = str(user_data['phone_number'])
        user_obj.save()
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Get Address List API: /pick/ or /drop/

Input:
auth_token

Output:
array
err: None if success, <string>
"""
@csrf_exempt
def pickrr_fetch_address_book(request,address_type):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        auth_token = str(user_data['auth_token'])
        user_obj = get_or_none(MainUser,mobile_token=auth_token,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid authentication token'
            return JsonResponse(response_data)
        if address_type == 'pick':
            all_pick_obj = user_obj.pickupaddress_set.all()
            return JsonQueryset(all_pick_obj)
        else:
            all_drop_obj = user_obj.deliveryaddress_set.all()
            return JsonQueryset(all_drop_obj)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Save Address API

Input:
auth_token
address_type: 0(pick), 1(drop), 2(both pick and drop)
name
phone_number
address_line
pincode

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_save_address_api(request):
    response_data = {}
    if request.method=='POST':
        address_data = read_post_json(request.body)
        auth_token = str(address_data['auth_token'])
        user_obj = get_or_none(MainUser,mobile_token=auth_token,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid authentication token'
            return JsonResponse(response_data)
        address_type = str(address_data['address_type'])
        name = str(address_data['name'])
        phone_number = str(address_data['phone_number'])
        address_line = str(address_data['address_line'])
        pincode = str(address_data['pincode'])
        pincode_obj = get_or_none(Pincode,pincode=pincode)
        if not pincode_obj:
            city = 'NA'
            state = 'NA'
        else:
            city = pincode_obj.region.city.name
            state = pincode_obj.region.city.state.name
        #pickup address
        if address_type == '0':
            pickup_obj = PickupAddress.objects.create(user=user_obj,
                                        name=name,
                                        address_line=address_line,
                                        phone_number=phone_number,
                                        pin_code=pincode,
                                        city=city,
                                        state=state)
            response_data['err'] = None
            return JsonResponse(response_data)
        #delivery address
        elif address_type == '1':
            delivery_obj = DeliveryAddress.objects.create(user=user_obj,
                                        name=name,
                                        address_line=address_line,
                                        phone_number=phone_number,
                                        pin_code=pincode,
                                        city=city,
                                        state=state)
            response_data['err'] = None
            return JsonResponse(response_data)
        #both pickup and delivery address
        else:
            pickup_obj = PickupAddress.objects.create(user=user_obj,
                                        name=name,
                                        address_line=address_line,
                                        phone_number=phone_number,
                                        pin_code=pincode,
                                        city=city,
                                        state=state)
            delivery_obj = DeliveryAddress.objects.create(user=user_obj,
                                        name=name,
                                        address_line=address_line,
                                        phone_number=phone_number,
                                        pin_code=pincode,
                                        city=city,
                                        state=state)
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Edit Address API

Input:
auth_token
address_type: 0(pick), 1(drop), 2(both pick and drop)
address_id
name
phone_number
address_line
pincode
Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_edit_address_api(request):
    response_data = {}
    if request.method=='POST':
        address_data = read_post_json(request.body)
        auth_token = address_data['auth_token']
        user_obj = get_or_none(MainUser,mobile_token=str(auth_token),is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user authentication details'
            return JsonResponse(response_data)
        address_id = int(address_data['address_id'])
        address_type = str(address_data['address_type'])
        name = str(address_data['name'])
        phone_number = str(address_data['phone_number'])
        address_line = str(address_data['address_line'])
        pincode = str(address_data['pincode'])
        pincode_obj = get_or_none(Pincode,pincode=pincode)
        if not pincode_obj:
            city = 'NA'
            state = 'NA'
        else:
            city = pincode_obj.region.city.name
            state = pincode_obj.region.city.state.name
        #pickup address
        if address_type == '0':
            pickup_obj = get_or_none(PickupAddress,pk=address_id)
            pickup_obj.name = name
            pickup_obj.phone_number = phone_number
            pickup_obj.pin_code = pincode
            pickup_obj.city = city
            pickup_obj.state = state
            pickup_obj.address_line = address_line
            pickup_obj.save()
            response_data['err'] = None
            return JsonResponse(response_data)
        #delivery address
        elif address_type == '1':
            delivery_obj = get_or_none(DeliveryAddress,pk=address_id)
            delivery_obj.name = name
            delivery_obj.phone_number = phone_number
            delivery_obj.pin_code = pincode
            delivery_obj.city = city
            delivery_obj.state = state
            delivery_obj.address_line = address_line
            delivery_obj.save()
            response_data['err'] = None
            return JsonResponse(response_data)
        #both pickup and delivery address
        else:
            pickup_obj = get_or_none(PickupAddress,pk=address_id)
            pickup_obj.name = name
            pickup_obj.phone_number = phone_number
            pickup_obj.pin_code = pincode
            pickup_obj.city = city
            pickup_obj.state = state
            pickup_obj.address_line = address_line
            pickup_obj.save()
            delivery_obj = get_or_none(DeliveryAddress,pk=address_id)
            delivery_obj.name = name
            delivery_obj.phone_number = phone_number
            delivery_obj.pin_code = pincode
            delivery_obj.city = city
            delivery_obj.state = state
            delivery_obj.address_line = address_line
            delivery_obj.save()
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Coupon Code API
Input:
from_address_id
to_address_id
coupon_code

Output:
discount %
max_discount
err: None if success, <string>
"""
@csrf_exempt
def pickrr_coupon_api(request):
    response_data ={}
    if request.method=='POST':
        coupon_data = read_post_json(request.body)
        code = coupon_data['coupon_code']
        if code == 'PICKRR50':
            response_data['discount'] = 50
            response_data['max_discount'] = 50
            response_data['err'] = None
            return JsonResponse(response_data)
        else:
            response_data['err'] = 'Invalid coupon code'
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Order Placing API

Input: 
auth_token: email_id in case of guest_user
coupon_code
item_name
service_type : rush, premium, standard
order_time : current+1hr in case of pickup for now else the datetimestamp chosen by user: Format: 2015-09-04 06:00
from_address_id
to_address_id
if no_address_id:
from_name
from_phone_number
from_pincode
from_address #address line
to_name
to_phone_number
to_pincode
to_address

Output:
order_id
tracking_id
pickrr_name
pickrr_phone_number
pickrr_photo
err
"""
@csrf_exempt
def pickrr_order_placing_api(request):
    response_data = {}
    if request.method=='POST':
        order_data = read_post_json(request.body)
        if 'email' in order_data:
            #guest-user
            user_obj,created = MainUser.objects.get_or_create(email=str(order_data['email']),is_guest=True)
        if 'auth_token' in order_data:
            #registered user
            user_obj = get_or_none(MainUser,mobile_token=str(order_data['auth_token']),is_guest=False)
            if not user_obj:
                response_data['err'] = 'Invalid authentication token'
                return JsonResponse(response_data)
        if 'coupon_code' in order_data:
            coupon_code = str(order_data['coupon_code'])
        item_name = str(order_data['item_name'])
        #rush, premium and standard
        service_type = str(order_data['service_type'])
        order_time = order_data['order_time']
        if 'from_address_id' in order_data:
            from_address_id = order_data['from_address_id']
            pickup_address = get_or_none(PickupAddress,pk=int(from_address_id))
            if not pickup_address:
                response_data['err'] = 'Invalid address details of the user'
                return JsonResponse(response_data)
            if pickup_address.user.pk != user_obj.pk:
                response_data['err'] = 'Invalid address details of the user'
                return JsonResponse(response_data)
        else:
            from_name = str(order_data['from_name'])
            from_phone_number = str(order_data['from_phone_number'])
            from_pincode = str(order_data['from_pincode'])
            from_address = str(order_data['from_address'])
            from_pin_code = get_or_none(Pincode,pincode=from_pincode)
            if not from_pin_code:
                from_city = 'NA'
                from_state = 'NA'
            else:
                from_city = from_pin_code.region.city.name
                from_state = from_pin_code.region.city.state.name
            pickup_address = PickupAddress.objects.create(user=user_obj,
                                                        name=from_name,
                                                        address_line=from_address,
                                                        city=from_city,
                                                        state=from_state,
                                                        pin_code=from_pincode,
                                                        phone_number=from_phone_number)
        if 'to_address_id' in order_data:
            to_address_id = order_data['to_address_id']
            delivery_address = get_or_none(DeliveryAddress,pk=int(to_address_id))
            if not delivery_address:
                response_data['err'] = 'Invalid address details of the user'
                return JsonResponse(response_data)
            if delivery_address.user.pk != user_obj.pk:
                response_data['err'] = 'Invalid address details of the user'
                return JsonResponse(response_data)
            to_pin_code = get_or_none(Pincode,pincode=str(delivery_address.pin_code))
            """
            to_city_obj = get_or_none(City,name=delivery_address.city)
            if not to_city_obj:
                to_city_obj = get_or_none(City,name='NA')
                if not to_city_obj:
                    response_data['err'] = 'Something went wrong'
                    return JsonResponse(response_data)
            """
        else:
            to_name = str(order_data['to_name'])
            to_phone_number = str(order_data['to_phone_number'])
            to_pincode = str(order_data['to_pincode'])
            to_address = str(order_data['to_address'])
            to_pin_code = get_or_none(Pincode,pincode=to_pincode)
            if not to_pin_code:
                to_city = 'NA'
                to_state = 'NA'
                """
                to_city_obj = get_or_none(City,name='NA')
                if not to_city_obj:
                    response_data['err'] = 'Something went wrong'
                    return JsonResponse(response_data)
                """
            else:
                to_city = from_pin_code.region.city.name
                to_state = from_pin_code.region.city.state.name
                #to_city_obj = to_pin_code.region.city
            delivery_address = DeliveryAddress.objects.create(user=user_obj,
                                                        name=to_name,
                                                        address_line=to_address,
                                                        city=to_city,
                                                        state=to_state,
                                                        pin_code=to_pincode,
                                                        phone_number=to_phone_number)
        if service_type == 'rush':
            service_name = 'Rush'
        elif service_type == 'premium':
            service_name = 'Premium'
        else:
            service_name = 'Standard'
        if not to_pin_code:
            zone = get_or_none(PickrrZone,zone_name='ROI')
            if not zone:
                response_data['err'] = 'Something went wrong'
                return JsonResponse(response_data)
        elif to_pin_code.region.city.from_service:
            zone = get_or_none(PickrrZone,zone_name='Intracity')
            if not zone:
                response_data['err'] = 'Something went wrong'
                return JsonResponse(response_data)
        else:
            zone = to_pin_code.region.city.pickrr_zone
            if not zone:
                response_data['err'] = 'Something went wrong'
                return JsonResponse(response_data)
        service_used = get_or_none(PickrrServices,
                                        service_zone=zone,
                                        service_name=service_name)
        order = Order.objects.create(user = user_obj,
                                    pickup_address = pickup_address,
                                    drop_address = delivery_address,
                                    service_used = service_used)
        #save item description
        product_obj = Product.objects.create(name=item_name)
        item_obj = OrderItems.objects.create(order=order,product=product_obj)
        order.order_id = str(1024 + order.pk)
        a = Pickrrs.objects.all()
        order.assigned_to = a[0]
        order.scheduled_for = order_time
        order.save()
        track_obj,is_created = Status.objects.get_or_create(order=order)
        track_id = track_obj.tracking_id
        track_obj.tracking_id = track_id[0:3] + str(order.pk) + track_id[3:8]
        track_obj.save()
        #order alert to our pickrrs
        subject = "#" + str(order.order_id) + " Order placed by " + str(order.user.email)
        if 'coupon_code' in order_data:
            body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order,'coupon_code':coupon_code})
        else:
            body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order})
        email = EmailMessage(subject, body, to=[order.assigned_to.email_id])
        email.content_subtype = "html"
        email.send()
        #successfully order placed info to our customers
        subject = "#" + str(order.order_id) + " Order succesfully placed"
        if 'coupon_code' in order_data:
            body = render_to_string('users/order_placed_email.html', {'order':order,'track_obj':track_obj,'coupon_code':coupon_code})
        else:
            body = render_to_string('users/order_placed_email.html', {'order':order,'track_obj':track_obj})
        email = EmailMessage(subject, body, to=[order.user.email])
        email.content_subtype = "html"
        email.send()
        if order.service_used.service_zone.zone_name == "Intracity":
            order_service_used = order.service_used.service_name + ' | Intracity'
        else:
            order_service_used = order.service_used.service_name + ' | Inter-city'
        response_data['order_id'] = order.order_id
        response_data['tracking_id'] = track_obj.tracking_id
        response_data['pickup_time'] = order_time
        response_data['pickrr_name'] = order.assigned_to.name
        response_data['pickrr_phone_number'] = order.assigned_to.phone_number
        response_data['order_service_used'] = order_service_used
        response_data['item_name'] = item_name
        if 'coupon_code' in order_data:
            response_data['coupon_code'] = coupon_code
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Order Cancellation API

Input:
order_id
auth_token in case of user

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_order_cancelling_api(request):
    response_data = {}
    if request.method=='POST':
        order_data = read_post_json(request.body)
        order_id = str(order_data['order_id'])
        order_obj = get_or_none(Order,order_id=order_id)
        if not order_obj:
            response_data['err'] = 'Sorry! No order found'
            return JsonResponse(response_data)
        order_obj.is_cancelled = True
        order_obj.save()
        track_obj = get_or_none(Status,order__id=order_obj.pk)
        if not track_obj:
            response_data['err'] = 'Sorry! No order found'
            return JsonResponse(response_data)
        subject = "Cancellation of order " + "#" + str(order_obj.order_id) + " by " + str(order_obj.user.email)
        body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order_obj})
        email = EmailMessage(subject, body, to=[order_obj.assigned_to.email_id])
        email.content_subtype = "html"
        email.send()
        #successfully order placed info to our customers
        subject = "Successfull cancellation of your order " + "#" + str(order_obj.order_id)
        body = render_to_string('users/order_placed_email.html', {'order':order_obj,'track_obj':track_obj})
        email = EmailMessage(subject, body, to=[order_obj.user.email])
        email.content_subtype = "html"
        email.send()
        #send sms to our customers
        destination_number = clean_phone_number(order_obj.pickup_address.phone_number)
        first_line = 'Dear customer' + '\n' 
        second_line = ('Your Order #',
                        str(order_id),
                        'at Pickrr is successfully cancelled.',
                        'Would love to serve you next time at http://www.pickrr.com')
        second_line = ' '.join(second_line)
        second_line = second_line + '.'
        message = first_line + second_line
        send_sms(destination_number,message)
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Input:
auth_token
old_password
new_password

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_change_password_api(request):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        auth_token = str(user_data['auth_token'])
        old_password = str(user_data['old_password'])
        new_password = str(user_data['new_password'])
        user_obj = get_or_none(MainUser,mobile_token=auth_token,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user authentication details'
            return JsonResponse(response_data)
        user = user_obj.user
        if not check_password(old_password,user.password):
            response_data['err'] = 'Password did not match'
            return JsonResponse(response_data)
        else:
            user.set_password(new_password)
            user.save()
            response_data['err'] = None
            return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Forgot Password API

Input:
email

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_forgot_password_api(request):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        email = str(user_data['email'])
        #new_password = str(user_data['new_password'])
        user_obj = get_or_none(MainUser,email=email,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid Email ID. Please Register'
            return JsonResponse(response_data)
        #user = user_obj.user
        #user.set_password(new_password)
        #user.save()
        subject = 'Pickrr - Link for setting password'
        set_link = 'http://www.pickrr.com/api/forgot-set-password/' + str(user_obj.mobile_token) + '/'
        body = render_to_string('users/set_password_email.html', {'user_obj':user_obj,'set_link':set_link})
        email = EmailMessage(subject, body, to=[email])
        email.content_subtype = "html"
        email.send()
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)

"""
Forgot-Set Password API

Input:
new_password (confirm it )

Output:
err: None if success, <string>
"""
@csrf_exempt
def pickrr_forgot_set_password_api(request,secret_key):
    user_obj = get_or_none(MainUser,mobile_token=str(secret_key),is_guest=False)
    if not user_obj:
        response_data['err'] = 'Wrong Place! Go to www.pickrr.com and happy shipping!'
        return JsonResponse(response_data)
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        new_password = str(user_data['new_password'])
        user = user_obj.user
        user.set_password(new_password)
        user.save()
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = None
        return JsonResponse(response_data)

"""
Personal Info API
URL: http://www.pickrr.com/api/personal-info/

Input:
auth_token

Output:
Name
Phone Number
Email
err: None if success, <string>
"""
@csrf_exempt
def pickrr_personal_info_api(request):
    response_data = {}
    if request.method=='POST':
        user_data = read_post_json(request.body)
        auth_token = str(user_data['auth_token'])
        user_obj = get_or_none(MainUser,mobile_token=auth_token,is_guest=False)
        if not user_obj:
            response_data['err'] = 'Invalid user authentication details'
            return JsonResponse(response_data)
        response_data['name'] = user_obj.name
        response_data['phone_number'] = user_obj.phone_number
        response_data['email'] = user_obj.email
        response_data['err'] = None
        return JsonResponse(response_data)
    else:
        response_data['err'] = 'Hey, Go to www.pickrr.com as here GET requests are not supported'
        return JsonResponse(response_data)