from django.conf.urls import patterns, url
from pickrr_common_api import views

urlpatterns = patterns('',
    url(r'^register-user/$',views.pickrr_registration_api,name='pickrr_registration_api'),
    url(r'^otp-auth/$',views.pickrr_otp_auth_api,name='pickrr_otp_auth_api'),
    url(r'^otp-resend/$',views.pickrr_otp_resend_api,name='pickrr_otp_resend_api'),
    url(r'^web-login/$',views.pickrr_web_login_api,name='pickrr_web_login_api'),
    url(r'^app-login/$',views.pickrr_app_login_api,name='pickrr_app_login_api'),
    url(r'^service-pricing/$',views.pickrr_service_pricing_api,name='pickrr_service_pricing_api'),
    url(r'^tracking/(?P<tracking_id>.+)/$',views.pickrr_tracking_api,name='pickrr_tracking_api'),
    url(r'^order-history/$',views.pickrr_user_order_history_api,name='pickrr_user_order_history_api'),
    url(r'^user-info-edit/$',views.pickrr_edit_user_info_api,name='pickrr_edit_user_info_api'),
    url(r'^fetch-address-book/(?P<address_type>.+)/$',views.pickrr_fetch_address_book,name='pickrr_fetch_address_book'),
    url(r'^save-address-book/$',views.pickrr_save_address_api,name='pickrr_save_address_api'),
    url(r'^coupon-code/$',views.pickrr_coupon_api,name='pickrr_coupon_api'),
    url(r'^place-order/$',views.pickrr_order_placing_api,name='pickrr_order_placing_api'),
    url(r'^cancel-order/$',views.pickrr_order_cancelling_api,name='pickrr_order_cancelling_api'),
    url(r'^change-password/$',views.pickrr_change_password_api,name='pickrr_change_password_api'),
    url(r'^forgot-password/$',views.pickrr_forgot_password_api,name='pickrr_forgot_password_api'),
    url(r'^forgot-set-password/(?P<secret_key>.+)/$',views.pickrr_forgot_set_password_api,name='pickrr_forgot_set_password_api'),
    url(r'^edit-address/$',views.pickrr_edit_address_api,name='pickrr_edit_address_api'),
    url(r'^personal-info/$',views.pickrr_personal_info_api,name='pickrr_personal_info_api'),
)