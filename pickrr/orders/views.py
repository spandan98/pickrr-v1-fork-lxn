from django.shortcuts import render_to_response, get_object_or_404, redirect, get_list_or_404, Http404
from django.core.urlresolvers import reverse
from django.template import Context, RequestContext
from django.core.context_processors import csrf
from orders.forms import UserTypeForm, CheckoutLoginForm, CheckoutSignupForm, OrderDataUploadForm, BluedartWaybillForm
from users.models import MainUser, PickupAddress, DeliveryAddress
from users.forms import PickupAddressForm, DeliveryAddressForm
from orders.models import *
from pricingmodel.models import PickrrServices, PickrrZone
from pickrrs.models import Pickrrs
import random
from products.models import Product
from django.contrib.auth import authenticate, login, get_user_model
from django.http import HttpResponse
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from track.models import Status
from locations.models import City, Pincode
from helpers.helpers import JsonResponse, read_post_json, get_or_none, bluedart_generate_waybill
from locations.models import Pincode
from django.views.decorators.csrf import csrf_exempt
from users.views import key_gen
import datetime

User = get_user_model()

def is_session_variable(value,request):
    return request.session.get(value,False)

#Checkout flow
def checkout_flow(request):
    """
    Determine guest user or registered user
    """
    user_type_form = UserTypeForm()
    if request.method=='POST':
        user_type_form = UserTypeForm(request.POST)
        if user_type_form.is_valid():
            user_type = user_type_form.cleaned_data['proceed']
            if user_type == 'guest':
                user,created = MainUser.objects.get_or_create(email=user_type_form.cleaned_data['email_field'],
                                is_guest=True)
                request.session['user_id'] = user.pk
                return redirect('orders:checkout_pickup_address')
            elif user_type == 'login_user':
                request.session['user_email'] = user_type_form.cleaned_data['email_field']
                return redirect('orders:checkout_login_user')
            else:
                request.session['user_email'] = user_type_form.cleaned_data['email_field']
                return redirect('orders:checkout_register_user')
    return render_to_response('orders/checkout_flow.html',
                            {'user_type_form':user_type_form},
                            context_instance=RequestContext(request))

def checkout_login_user(request):
    if is_session_variable('user_email',request):
        email = request.session['user_email']
    else:
        raise Http404
    user,created = MainUser.objects.get_or_create(email=email,is_guest=False)
    checkout_login_form = CheckoutLoginForm(user=user)
    if request.method=='POST':
        checkout_login_form = CheckoutLoginForm(request.POST,user=user)
        if checkout_login_form.is_valid():
            password = checkout_login_form.cleaned_data['password']
            main_user = authenticate(username=user.user.username, password=password)
            if main_user is not None:
                if main_user.is_active:
                    login(request,main_user)
                    #successful login
                    request.session['user_id'] = user.pk
                    return redirect('orders:checkout_pickup_address')
                else:
                    return HttpResponse('Your account has been deactivated. Please contact 09818197991')
            else:
                #invalid email or password
                return HttpResponse('invalid email or password')
    return render_to_response('orders/checkout_login_user.html',
                            {'checkout_login_form':checkout_login_form},
                            context_instance=RequestContext(request))

def checkout_register_user(request):
    if is_session_variable('user_email',request):
        email = request.session['user_email']
    else:
        raise Http404
    checkout_signup_form = CheckoutSignupForm(email=email)
    if request.method=='POST':
        checkout_signup_form = CheckoutSignupForm(request.POST,email=email)
        if checkout_signup_form.is_valid():
            password = checkout_signup_form.cleaned_data['password']
            signup,created = MainUser.objects.get_or_create(email=email,
                                                            is_guest=False)
            if created:
                #Max length of auth user is 30
                username = 'mainuser' + str(signup.pk)
                #creating a user object corresponding to the customer user
                user = User.objects.create_user(username,signup.email,password)
                user.is_active = True 
                user.save()
                signup.user = user
                signup.save()
            else:
                username = signup.user.username
            #No verification email for now
            #Authenticate the user and allow login directly
            main_user = authenticate(username=username, password=password)
            if main_user is not None:
                if main_user.is_active:
                    login(request,main_user)
                    #successful login
                    request.session['user_id'] = signup.pk
                    return redirect('orders:checkout_pickup_address')
                else:
                    return HttpResponse('Your account has been deactivated. Please contact 09818197991')
            else:
                #invalid email or password
                return HttpResponse('invalid email or password')
    return render_to_response('orders/checkout_register_user.html',
                            {'checkout_signup_form':checkout_signup_form},
                            context_instance=RequestContext(request))

def checkout_pickup_address(request):
    #print request.session['user_id']
    #print request.session['from_location_id']
    if is_session_variable('user_id',request):
        user = get_object_or_404(MainUser,pk=request.session['user_id'])
    else:
        raise Http404
    #print "hello"
    if request.session.get('from_location_id',False):
        pickup_add_form = PickupAddressForm(initial={'from_city':request.session['from_location_id']})
    else:
        pickup_add_form = PickupAddressForm()
    if 'add_pickup' in request.POST:
        pickup_add_form = PickupAddressForm(request.POST)
        if pickup_add_form.is_valid():
            pickup_info = pickup_add_form.save(commit=False)
            pickup_info.user=user
            from_city = pickup_add_form.cleaned_data['from_city']
            #from_state = pickup_add_form.cleaned_data['from_state']
            pickup_info.city = dict(pickup_add_form.fields['from_city'].choices)[int(from_city)]
            pickup_info.state = get_object_or_404(City,pk=int(from_city)).state.name
            pickup_info.save()
            request.session['pickup_address_id'] = pickup_info.pk
            return redirect('orders:checkout_delivery_address')
    if 'pickup_select' in request.POST:
        pickup_address_id = request.POST.get('select_pickup')
        if not pickup_address_id:
            raise Http404
        request.session['pickup_address_id'] = pickup_address_id
        return redirect('orders:checkout_delivery_address')
    #print user.pk
    return render_to_response('orders/checkout_pickup_address.html',
                            {'pickup_add_form':pickup_add_form,
                            'user':user},
                            context_instance=RequestContext(request))

def checkout_delivery_address(request):
    if is_session_variable('user_id',request):
        user = get_object_or_404(MainUser,pk=request.session['user_id'])
    else:
        raise Http404
    #print "hello"
    if request.session.get('to_location_id',False):
        delivery_add_form = DeliveryAddressForm(initial={'to_city':request.session['to_location_id']})
    else:
        delivery_add_form = DeliveryAddressForm()
    #print request.session.get('pickup_address__id',False)
    if 'add_delivery' in request.POST:
        delivery_add_form = DeliveryAddressForm(request.POST)
        if delivery_add_form.is_valid():
            delivery_info = delivery_add_form.save(commit=False)
            delivery_info.user=user
            to_city = delivery_add_form.cleaned_data['to_city']
            #to_state = delivery_add_form.cleaned_data['to_state']
            delivery_info.city = dict(delivery_add_form.fields['to_city'].choices)[int(to_city)]
            delivery_info.state = get_object_or_404(City,pk=int(to_city)).state.name
            delivery_info.save()
            pickup_address = get_object_or_404(PickupAddress,pk=request.session.get('pickup_address_id',False))
            service_used = get_object_or_404(PickrrServices,pk=request.session.get('service_id',False))
            order = Order.objects.create(user = user,
                                        pickup_address = pickup_address,
                                        drop_address = delivery_info,
                                        service_used = service_used)
            order.order_id = str(1024 + order.pk)
            order.save()
            track_obj,is_created = Status.objects.get_or_create(order=order)
            track_id = track_obj.tracking_id
            track_obj.tracking_id = track_id[0:3] + str(order.pk) + track_id[3:8]
            if not track_obj.key:
                track_obj.key = key_gen(track_obj.pk)
            track_obj.save()
            """
            order_items = OrderItems.objects.create(order=order,
                                                product=get_object_or_404(Product,pk=request.session.get('product_id',False)))
            """
            request.session['order_id'] = order.pk
            return redirect('orders:pickup_order_placed')
    if 'delivery_select' in request.POST:
        delivery_address_id = request.POST.get('select_delivery')
        if not delivery_address_id:
            raise Http404
        delivery_address = get_object_or_404(DeliveryAddress,pk=delivery_address_id)
        pickup_address = get_object_or_404(PickupAddress,pk=request.session.get('pickup_address_id',False))
        service_used = get_object_or_404(PickrrServices,pk=request.session.get('service_id',False))
        order = Order.objects.create(user = user,
                                    pickup_address = pickup_address,
                                    drop_address = delivery_address,
                                    service_used = service_used)
        order.order_id = str(1024 + order.pk)
        order.save()
        track_obj,is_created = Status.objects.get_or_create(order=order)
        track_id = track_obj.tracking_id
        track_obj.tracking_id = track_id[0:3] + str(order.pk) + track_id[3:8]
        if not track_obj.key:
            track_obj.key = key_gen(track_obj.pk)
        track_obj.save()
        """
        order_items = OrderItems.objects.create(order=order,
                                                product=get_object_or_404(Product,pk=request.session.get('product_id',False)))
        """
        request.session['order_id'] = order.pk
        return redirect('orders:pickup_order_placed')
    #print user.pk
    return render_to_response('orders/checkout_delivery_address.html',
                            {'delivery_add_form':delivery_add_form,
                            'user':user},
                            context_instance=RequestContext(request))

def pickup_order_placed(request):
    order_id = request.session.get('order_id',False)
    if not order_id:
        raise Http404
    """
    for key in request.session.keys():
        del request.session[key]
    """
    #Delete specific keys in request.session
    if request.session.get('order_id',False):
        del request.session['order_id']
    if request.session.get('from_location_id',False):
        del request.session['from_location_id']
    if request.session.get('to_location_id',False):
        del request.session['to_location_id']
    #del request.session['product_id']
    if request.session.get('service_id',False):
        del request.session['service_id']
    if request.session.get('user_id',False):
        del request.session['user_id']
    if request.session.get('pickup_address_id',False):
        del request.session['pickup_address_id']
    if request.session.get('user_email',False):
        del request.session['user_email']
    a = list(Pickrrs.objects.all())
    random.shuffle(a)
    #print a
    order = get_object_or_404(Order,pk=order_id)
    order.assigned_to = a[0]
    order.save()
    track_obj = get_object_or_404(Status,order__id=order.pk)
    if order.user.no_email:
        pass
    else:
        #order alert to our pickrrs
        if order.is_cancelled:
            subject = "Cancellation of order " + "#" + str(order.order_id) + " by " + str(order.user.email)
        else:
            subject = "#" + str(order.order_id) + " Order placed by " + str(order.user.email)
        body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order})
        email = EmailMessage(subject, body, to=[order.assigned_to.email_id])
        email.content_subtype = "html"
        email.send()
        #successfully order placed info to our customers
        if order.is_cancelled:
            subject = "Successfull cancellation of your order " + "#" + str(order.order_id)
        else:
            subject = "#" + str(order.order_id) + " Order succesfully placed"
        body = render_to_string('users/order_placed_email.html', {'order':order,'track_obj':track_obj})
        email = EmailMessage(subject, body, to=[order.user.email])
        email.content_subtype = "html"
        email.send()
    return render_to_response('orders/pickup_order_placed.html',
                            {'order':order,
                            'track_obj':track_obj},
                            context_instance=RequestContext(request))

#Call to cancel the order
def cancel_order(request,order_id):
    order_obj = get_object_or_404(Order,pk=order_id)
    order_obj.is_cancelled = True
    order_obj.save()
    request.session['order_id'] = order_obj.pk
    return redirect('orders:pickup_order_placed')

"""
def testing_date_json(request):
    import json
    date_json = json.dumps({
        "point_in_time": "2012-09-04 06:00"
    })
    myval = json.loads(date_json)
    myorder = get_object_or_404(Order,pk=51)
    myorder.scheduled_for = myval['point_in_time']
    myorder.save()
    return JsonResponse({'message':'success'})
"""

#API for placing orders from mobile app
@csrf_exempt
def order_placing_api(request):
    #print request
    if request.POST:
        order_data = read_post_json(request.body)
        #order_data = request.body
        #print order_data
        user_id = order_data['user_id']
        user_obj = get_object_or_404(MainUser,pk=int(user_id))
        auth_token = order_data['auth_token']
        if str(auth_token) != str(user_obj.mobile_token):
            raise Http404
        #0-rush,1-premium,2-standard
        service_type = order_data['service_type']
        order_time = order_data['order_time']
        if 'from_address_id' in order_data:
            from_address_id = order_data['from_address_id']
            pickup_address = get_object_or_404(PickupAddress,pk=int(from_address_id))
            if pickup_address.user.pk != user_obj.pk:
                raise Http404
            #from_city_obj = get_object_or_404(City,name=pickup_address.city)
        else:
            from_name = order_data['from_name']
            from_phone_number = order_data['from_phone_number']
            from_pincode = order_data['from_pincode']
            from_address = order_data['from_address']
            from_pin_code = get_or_none(Pincode,pincode=from_pincode)
            if not from_pin_code:
                response_data = {
                    'order_id' : '',
                    'tracking_id' : '',
                    'pickup_time' : '',
                    'pickrr_name' : '',
                    'pickrr_phone_number' : '',
                    'order_service_used' : '',
                    'error_message' : 'Invalid Pincode in Pickup Address'
                }
                return JsonResponse(response_data)
            #from_city_obj = from_pin_code.region.city
            from_city = from_pin_code.region.city.name
            from_state = from_pin_code.region.city.state.name
            pickup_address = PickupAddress.objects.create(user=user_obj,
                                                        name=from_name,
                                                        address_line=from_address,
                                                        city=from_city,
                                                        state=from_state,
                                                        pin_code=from_pin_code,
                                                        phone_number=from_phone_number)
        if 'to_address_id' in order_data:
            to_address_id = order_data['to_address_id']
            delivery_address = get_object_or_404(DeliveryAddress,pk=int(to_address_id))
            if delivery_address.user.pk != user_obj.pk:
                raise Http404
            to_city_obj = get_object_or_404(City,name=delivery_address.city)
        else:
            to_name = order_data['to_name']
            to_phone_number = order_data['to_phone_number']
            to_pincode = order_data['to_pincode']
            to_address = order_data['to_address']
            to_pin_code = get_or_none(Pincode,pincode=to_pincode)
            if not to_pin_code:
                response_data = {
                    'order_id' : '',
                    'tracking_id' : '',
                    'pickup_time' : '',
                    'pickrr_name' : '',
                    'pickrr_phone_number' : '',
                    'order_service_used' : '',
                    'error_message' : 'Invalid Pincode in Destination Address'
                }
                return JsonResponse(response_data)
            to_city_obj = to_pin_code.region.city
            to_city = to_city_obj.name
            to_state = to_pin_code.region.city.state.name
            delivery_address = DeliveryAddress.objects.create(user=user_obj,
                                                        name=to_name,
                                                        address_line=to_address,
                                                        city=to_city,
                                                        state=to_state,
                                                        pin_code=to_pin_code,
                                                        phone_number=to_phone_number)
        if service_type == '0':
            service_name = 'Rush'
        elif service_type == '1':
            service_name = 'Premium'
        else:
            service_name = 'Standard'
        if to_city_obj.from_service and to_city_obj.to_service:
            zone = get_object_or_404(PickrrZone,zone_name='Intracity')
        else:
            zone = to_city_obj.pickrr_zone 
        service_used = get_object_or_404(PickrrServices,
                                        service_zone=zone,
                                        service_name=service_name)
        order = Order.objects.create(user = user_obj,
                                    pickup_address = pickup_address,
                                    drop_address = delivery_address,
                                    service_used = service_used)
        order.order_id = str(1024 + order.pk)
        a = Pickrrs.objects.all()
        order.assigned_to = a[0]
        order.scheduled_for = order_time
        order.save()
        track_obj,is_created = Status.objects.get_or_create(order=order)
        track_id = track_obj.tracking_id
        track_obj.tracking_id = track_id[0:3] + str(order.pk) + track_id[3:8]
        track_obj.save()
        #order alert to our pickrrs
        subject = "#" + str(order.order_id) + " Order placed by " + str(order.user.email)
        body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order})
        email = EmailMessage(subject, body, to=[order.assigned_to.email_id])
        email.content_subtype = "html"
        email.send()
        #successfully order placed info to our customers
        subject = "#" + str(order.order_id) + " Order succesfully placed"
        body = render_to_string('users/order_placed_email.html', {'order':order,'track_obj':track_obj})
        email = EmailMessage(subject, body, to=[order.user.email])
        email.content_subtype = "html"
        email.send()
        if order.service_used.service_zone.zone_name == "Intracity":
            order_service_used = order.service_used.service_name + ' | Intracity'
        else:
            order_service_used = order.service_used.service_name + ' | Inter-city'
        response_data = {
            'order_id' : order.order_id,
            'tracking_id' : track_obj.tracking_id,
            'pickup_time' : order_time,
            'pickrr_name' : order.assigned_to.name,
            'pickrr_phone_number' : order.assigned_to.phone_number,
            'order_service_used' : order_service_used,
            'error_message' : ''
        }
        #print JsonResponse(response_data)
        return JsonResponse(response_data)
    else:
        raise Http404

#API returns all orders of a particular user
def user_orders_api(request,user_id,auth_token):
    user_obj = get_object_or_404(MainUser,pk=int(user_id))
    if str(user_obj.mobile_token) != str(auth_token):
        raise Http404
    order_list = user_obj.order_set.all()
    response_list = []
    for order in order_list:
        track_obj = get_or_none(Status,order=order)
        if not track_obj:
            tracking_id = ''
        else:
            tracking_id = track_obj.tracking_id
        if order.invoice_sent:
            bill = 'Rs. ' + str(order.total_bill)
        else:
            bill = 'In Process'
        order_dict = {
            'order_id' : order.order_id,
            'tracking_id' : tracking_id,
            'from_city' : order.pickup_address.city,
            'to_city' : order.drop_address.city,
            'placed_on' : order.placed_at.strftime("%B %d, %y, %H:%M"),
            'bill' : bill,
        }
        response_list.append(order_dict)
    return JsonResponse(response_list)

def order_data_upload_ops(request):
    order_form = OrderDataUploadForm()
    if request.method=='POST':
        order_form = OrderDataUploadForm(request.POST,request.FILES)
        if order_form.is_valid():
            user_account = order_form.cleaned_data['user_account']
            user = get_object_or_404(MainUser,email=user_account,is_guest=False)
            pickup_address = user.pickupaddress_set.all()[0]
            order_data = request.FILES['order_data'].read()
            """
            #Input Data
            Date: yyyy-mm-dd hh:mm(0)
            Order_no (order_id):(1)
            Recepient:(2)
            Pincode:(3)-7
            Tracking ID:(4)-6
            Length:(5)-5
            Breadth(6)-4
            Height(7)-3
            Actual Weight(8)-2
            To calculate(not input)Only required in invoice--Effective Weight*(lbh/5000)
            Price(9)-1
            """
            order_data = order_data.split('\n')
            try:
                order_data.remove('')
            except ValueError:
                pass
            for order_info in order_data:
                order_info = order_info.split(',')
                address_hack = ''
                for i in range(2,len(order_info)-7):
                    address_hack += order_info[i]
                    if i < (len(order_info)-8):
                        address_hack += ';'
                #print address_hack
                #print order_info[2].split(',')[0]
                #print order_info[-7]
                drop_address,created = DeliveryAddress.objects.get_or_create(user=user,
                                                                            name=address_hack.split(';')[0],
                                                                            address_line=address_hack,
                                                                            city=address_hack.split(';')[-2],
                                                                            state=address_hack.split(';')[-2],
                                                                            pin_code=order_info[-7],
                                                                            phone_number=address_hack.split(';')[-1].split(':')[1])
                order,created = Order.objects.get_or_create(order_id=order_info[1],
                                                            user=user,
                                                            pickup_address=pickup_address,
                                                            drop_address=drop_address,
                                                            service_used=PickrrServices.objects.get(pk=9),
                                                            placed_at=order_info[0],
                                                            total_bill=order_info[-1]
                                                            )
                track_obj,is_created = Status.objects.get_or_create(order=order)
                track_obj.tracking_id = order_info[-6]
                if not track_obj.key:
                    track_obj.key = key_gen(track_obj.pk)
                order.save()
                #archies timimg jugaad
                order.placed_at = order.placed_at.replace(hour=19, minute=5)
                order.save()
                track_obj.pickup_time = order.placed_at.replace(hour=19, minute=45)
                track_obj.warehouse_receive_time = order.placed_at.replace(hour=20, minute=57)
                new_date = order.placed_at + datetime.timedelta(days=1)
                track_obj.in_transit_time = new_date.replace(hour=9, minute=40)
                track_obj.in_transit_status = 'Reached ' + str(address_hack.split(';')[-2]) + ' hub'
                track_obj.out_delivery_time = new_date.replace(hour=10, minute=55)
                track_obj.save()
                #Timing jugaad for archies
                
                order_item, is_created = OrderItems.objects.get_or_create(order=order)
                order_item.length=float(order_info[-5])
                order_item.breadth=float(order_info[-4])
                order_item.height=float(order_info[-3])
                order_item.weight=float(order_info[-2])
                order_item.save()
            return redirect('orders:order_data_upload_ops')
    return render_to_response('orders/order_data_upload_ops.html',
                                {'order_form':order_form},
                                context_instance=RequestContext(request))

"""
Business API for placing orders for our B2B customers
"""
@csrf_exempt
def order_placing_business_api(request):
    response_data = {}
    #print request
    if request.POST:
        order_data = read_post_json(request.body)
        user_email = order_data['email']
        user_token = order_data['auth_token']
        user_obj = get_or_none(MainUser,email=user_email,is_guest=False)
        if not user_obj:
            response_data['message'] = 'User does not exist'
            response_data['tracking_id'] = 'Error'
            return JsonResponse(response_data)
        if str(user_token) != str(user_obj.mobile_token):
            response_data['message'] = 'Unauthorized user'
            response_data['tracking_id'] = 'Error'
            return JsonResponse(response_data)
        order_time = order_data['pickup_time']
        pickup_time = order_data['pickup_time']
        from_name = order_data['from_name']
        from_phone_number = order_data['from_phone_number']
        from_address = order_data['from_address_line']
        from_state = order_data['from_state']
        from_city = order_data['from_city']
        from_pincode = order_data['from_pincode']
        if len(str(from_pincode)) < 6 or len(str(from_pincode)) > 6:
            response_data['message'] = 'Invalid Pickup Address Pincode'
            response_data['tracking_id'] = 'Error'
            return JsonResponse(response_data)
        pickup_address,is_new = PickupAddress.objects.get_or_create(user=user_obj,
                                                                    name=from_name,
                                                                    address_line=from_address,
                                                                    city=from_city,
                                                                    state=from_state,
                                                                    pin_code=from_pincode,
                                                                    phone_number=from_phone_number)
        to_name = order_data['to_name']
        to_phone_number = order_data['to_phone_number']
        to_address = order_data['to_address_line']
        to_state = order_data['to_state']
        to_city = order_data['to_city']
        to_pincode = order_data['to_pincode']
        if len(str(to_pincode)) < 6 or len(str(to_pincode)) > 6:
            response_data['message'] = 'Invalid Delivery Address Pincode'
            response_data['tracking_id'] = 'Error'
            return JsonResponse(response_data)
        delivery_address,is_new = DeliveryAddress.objects.get_or_create(user=user_obj,
                                                                        name=to_name,
                                                                        address_line=to_address,
                                                                        city=to_city,
                                                                        state=to_state,
                                                                        pin_code=to_pincode,
                                                                        phone_number=to_phone_number)
        if 'cod_amount' in order_data:
            cod_amount = float(order_data['cod_amount'])
            order = Order.objects.create(user = user_obj,
                                    pickup_address = pickup_address,
                                    drop_address = delivery_address,
                                    is_cod=True,
                                    cod_amount=cod_amount)
        else:
            order = Order.objects.create(user = user_obj,
                                    pickup_address = pickup_address,
                                    drop_address = delivery_address)
        order.order_id = order_data['order_id']
        a = Pickrrs.objects.all()
        order.assigned_to = a[0]
        order.scheduled_for = order_time
        order.save()
        track_obj,is_created = Status.objects.get_or_create(order=order)
        if user_obj.tracking_code:
            track_obj.tracking_id = user_obj.tracking_code + order.order_id
        else:
            if user_obj.company_name:
                track_obj.tracking_id = str(user_obj.company_name.replace(' ',''))[:3] + order.order_id
            else:
                track_obj.tracking_id = 'PKR' + order.order_id
        track_obj.save()
        #order alert to our pickrrs
        subject = "#" + str(order.order_id) + " Order placed by " + str(order.user.email)
        is_business = True
        body = render_to_string('pickrrs/order_pickrr_email.html', {'order':order,'is_business':is_business})
        email = EmailMessage(subject, body, to=[order.assigned_to.email_id])
        email.content_subtype = "html"
        email.send()
        response_data['message'] = 'success'
        response_data['tracking_id'] = track_obj.tracking_id
        return JsonResponse(response_data)
    else:
        response_data['message'] = 'Expected a POST request'
        response_data['tracking_id'] = 'Error'
        return JsonResponse(response_data)

def bluedart_waybill_generate(request, order_id):
    waybill_form = BluedartWaybillForm()
    order = get_object_or_404(Order, order_id=order_id)
    if request.method=='POST':
        waybill_form = BluedartWaybillForm(request.POST)
        if waybill_form.is_valid():
            response_data = bluedart_generate_waybill(order, waybill_form)
            waybill_data = waybill_form.cleaned_data
            return render_to_response('orders/bluedart_response.html',{
                                        'response_data': response_data,
                                        'order': order,
                                        'waybill_data':waybill_data},
                                        context_instance=RequestContext(request))
    return render_to_response('orders/bluedart_waybill_generate.html',
                                {'waybill_form': waybill_form,
                                 'order': order},
                                context_instance=RequestContext(request))
