from django.conf.urls import patterns, url
from orders import views

urlpatterns = patterns('',
    url(r'^checkout/$',views.checkout_flow,name='checkout_flow'),
    url(r'^checkout/pickup/address/$',views.checkout_pickup_address,name='checkout_pickup_address'),
    url(r'^checkout/delivery/address/$',views.checkout_delivery_address,name='checkout_delivery_address'),
    url(r'^checkout/login/$',views.checkout_login_user,name='checkout_login_user'),
    url(r'^checkout/register/$',views.checkout_register_user,name='checkout_register_user'),
    url(r'^order-placed/$',views.pickup_order_placed,name='pickup_order_placed'),
    url(r'^place-order-app/$',views.order_placing_api,name='order_placing_api'),
    url(r'^cancel-order/(?P<order_id>.+)/$',views.cancel_order,name='cancel_order'),
    url(r'^order-placing-api/$',views.order_placing_business_api,name='order_placing_business_api'),
    url(r'^upload-order-data-ops/$',views.order_data_upload_ops,name='order_data_upload_ops'),
    url(r'^bluedart-waybill-generate/(?P<order_id>.+)/$', views.bluedart_waybill_generate, name='bluedart_waybill_generate'),
    url(r'^(?P<user_id>.+)/(?P<auth_token>.+)/user-orders-list/api/$',views.user_orders_api,name='user_orders_api'),
)