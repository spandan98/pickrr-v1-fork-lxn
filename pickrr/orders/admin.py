from django.contrib import admin
from orders.models import Order, OrderItems
    
class OrderAdmin(admin.ModelAdmin):
    list_display=('order_id','is_cancelled','get_tracking_id','get_user_name','get_user_phone','placed_at','get_pickup_address',
                'get_drop_address','get_service_used','get_pickrrs','total_bill',
                'invoice_sent')

    search_fields=('order_id','user__email','pickup_address__city','drop_address__city',
                    'assigned_to__name','service_used__service_name')

    def get_user_name(self,obj):
        if obj.user.name:
            return obj.user.name
        else:
            return obj.user.email

    def get_user_phone(self,obj):
        if obj.user.phone_number:
            return obj.user.phone_number
        else:
            return 'guest-user'

    def get_pickup_address(self,obj):
        mystr = "<br>".join([obj.pickup_address.name, obj.pickup_address.phone_number, 
                obj.pickup_address.address_line, obj.pickup_address.state, 
                obj.pickup_address.city, obj.pickup_address.pin_code])
        return mystr

    get_pickup_address.allow_tags = True

    def get_drop_address(self,obj):
        if obj.drop_address:
            mystr = "<br>".join([obj.drop_address.name, obj.drop_address.phone_number, 
                obj.drop_address.address_line, obj.drop_address.state, 
                obj.drop_address.city, obj.drop_address.pin_code])
            return mystr
    
    get_drop_address.allow_tags = True

    def get_pickrrs(self,obj):
        if obj.assigned_to:
            return obj.assigned_to.name

    def get_service_used(self,obj):
        if obj.service_used:
            if obj.service_used.service_zone.zone_name == "Intracity":
                return obj.service_used.service_name + ' | Intracity'
            else:
                return obj.service_used.service_name + ' | Inter-city'
        else:
            return 'Business Customer'

    def get_tracking_id(self,obj):
        from track.models import Status
        from helpers.helpers import get_or_none
        #track_obj = obj.status_set.all()
        track_obj = get_or_none(Status,order=obj)
        #print track_obj
        if track_obj:
            return track_obj.tracking_id

admin.site.register(Order,OrderAdmin)
admin.site.register(OrderItems)