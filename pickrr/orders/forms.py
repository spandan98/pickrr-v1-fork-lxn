from django import forms
from users.models import MainUser
from django.contrib.auth.hashers import check_password
from helpers.helpers import get_or_none, filter_or_none
from django.contrib.admin import widgets


class UserTypeForm(forms.Form):
    email_field = forms.EmailField(label="Enter Email ID",required=True)
    CHOICES=[('guest','As guest user'),
         ('login_user','Login'),
         ('register_user','Register (Recommended for future convenience)')]
    proceed = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect(),required=True)

class CheckoutLoginForm(forms.Form):
    password = forms.CharField(widget = forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(CheckoutLoginForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(CheckoutLoginForm, self).clean()
        user = self.user
        password = cleaned_data.get("password")
        if user and password:
            user = user.user
            if check_password(password,user.password):
                if not user.is_active:
                    msg = u"Your account has been deactivated.Please contact 09818197991 "
                    self._errors["password"] = self.error_class([msg])
                    # Password field is no longer valid.Thus, Remove it from the cleaned data.
                    del cleaned_data["email"]
            else:
                msg = u"Password did not match"
                self._errors["password"] = self.error_class([msg])
                # Password field is no longer valid.Thus, Remove it from the cleaned data.
                del cleaned_data["password"]
        # return full collection of cleaned data.
        return cleaned_data

class CheckoutSignupForm(forms.Form):
    password = forms.CharField(widget = forms.PasswordInput())
    confirm_password = forms.CharField(widget = forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        self.email = kwargs.pop('email', None)
        super(CheckoutSignupForm, self).__init__(*args, **kwargs)

    #Custom validation
    def clean(self):
        cleaned_data = super(CheckoutSignupForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")
        email = self.email
        if email:
            email_exist = get_or_none(MainUser,email=email,is_guest=False)
            if email_exist:
                #msg = u""
                #self._errors["email"] = self.error_class([msg])
                raise forms.ValidationError("This email ID is already registered.Either retrieve password or use different email ID")
                #del cleaned_data["email"]
        if password and confirm_password:
            if len(password) < 6:
                msg = u"Pasword must be of length greater than or equal to 6"
                self._errors["password"] = self.error_class([msg])
                del cleaned_data["password"]
            if password != confirm_password:
                msg = u"Pasword did not match"
                self._errors["confirm_password"] = self.error_class([msg])
                del cleaned_data["confirm_password"]
        return cleaned_data

class OrderDataUploadForm(forms.Form):
    user_account = forms.ModelChoiceField(queryset=MainUser.objects.none())
    #order_pickup_date = forms.DateTimeField(widget=widgets.AdminSplitDateTime)
    order_data = forms.FileField()

    def __init__(self, *args, **kwargs):
        #For dynamically changing the queryset of the field user_account!
        super(OrderDataUploadForm, self).__init__(*args, **kwargs)
        self.fields['user_account'].queryset = filter_or_none(MainUser,is_guest=False)


class BluedartWaybillForm(forms.Form):
    actual_weight = forms.DecimalField(label="Actual Weight")
    length = forms.IntegerField(label="Length")
    width = forms.IntegerField(label="Width")
    height = forms.IntegerField(label="Height")
    pickup_date_time = forms.DateTimeField()
    SUB_PRODUCT_CHOICES = [
        ("C", "Cash On Delivery"),
        ("P", "Prepaid")
    ]
    sub_product_type = forms.ChoiceField(choices=SUB_PRODUCT_CHOICES)
    PACKAGE_CHOICES = [
        ("Dutiables", "Dutiables"),
        ("Docs", "Documents")
    ]
    package_type = forms.ChoiceField(choices=PACKAGE_CHOICES)
    collectable_amount = forms.DecimalField(label="Collectable/Declared Amount")




