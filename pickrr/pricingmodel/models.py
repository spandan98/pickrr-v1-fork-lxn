from django.db import models

class PickrrZone(models.Model):
    zone_name = models.CharField(max_length=64)

    def __unicode__(self):
        return self.zone_name

class PickrrServices(models.Model):
    service_zone = models.ForeignKey(PickrrZone)
    SN = (('Rush','Rush'),
        ('Premium','Premium'),
        ('Standard','Standard'))
    service_name = models.CharField(max_length=64,choices=SN)
    #days or hours depending upon zone
    min_service_duration = models.IntegerField(default=0)
    max_service_duration = models.IntegerField(default=0)
    base_price = models.FloatField(default=0)
    base_weight = models.FloatField(default=0.5)
    add_price = models.FloatField(default=0)

    def __unicode__(self):
        mystring = self.service_name + '_' + self.service_zone.zone_name
        return mystring