# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PickrrZone'
        db.create_table(u'pricingmodel_pickrrzone', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zone_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'pricingmodel', ['PickrrZone'])

        # Adding model 'PickrrServices'
        db.create_table(u'pricingmodel_pickrrservices', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('service_zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pricingmodel.PickrrZone'])),
            ('service_name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('min_service_duration', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('max_service_duration', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('base_price', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('base_weight', self.gf('django.db.models.fields.FloatField')(default=0.5)),
            ('add_price', self.gf('django.db.models.fields.FloatField')(default=0)),
        ))
        db.send_create_signal(u'pricingmodel', ['PickrrServices'])


    def backwards(self, orm):
        # Deleting model 'PickrrZone'
        db.delete_table(u'pricingmodel_pickrrzone')

        # Deleting model 'PickrrServices'
        db.delete_table(u'pricingmodel_pickrrservices')


    models = {
        u'pricingmodel.pickrrservices': {
            'Meta': {'object_name': 'PickrrServices'},
            'add_price': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'base_price': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'base_weight': ('django.db.models.fields.FloatField', [], {'default': '0.5'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_service_duration': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'min_service_duration': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'service_name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'service_zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pricingmodel.PickrrZone']"})
        },
        u'pricingmodel.pickrrzone': {
            'Meta': {'object_name': 'PickrrZone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['pricingmodel']