from django.core.exceptions import ValidationError
import json
from django.http import HttpResponse
import random
import string
import hashlib
from django.core import serializers
from datetime import timedelta
from suds.client import Client
from suds.bindings import binding
from suds.sax.element import Element
import time
import uuid

def validate_pincode(value,length=6):
    if len(str(value))!=length:
        raise ValidationError(u'Invalid Pincode')

def validate_phone_number(value,length=10):
    if len(str(value)) < length:
        raise ValidationError(u'Invalid Phone Number')

#Wrapper which will be very helpful - return an object or None
def get_or_none(model, **kwargs):
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None

#Returns an empty list in the case of no objects found
def filter_or_none(model, **kwargs):
    "filters object or returns none"
    try:
        return model.objects.filter(**kwargs)
    except model.DoesNotExist:
        return None

#Plivo API
def send_sms(dst_number,body):
    import plivo
    auth_id = "MAYTBKMTBKZGM4NTEWOT"
    auth_token = "ZmE3NzhmM2MyZjRiN2RmYzRkMTlmYmQ2ZTlhNjYy"
    p = plivo.RestAPI(auth_id, auth_token)
    params = {
        'src': '+13046063278', # Sender's phone number with country code
        'dst' : dst_number, # Receiver's phone Number with country code
        'text' : body, # Your SMS Text Message - English
        'method' : 'POST' # The method used to call the url
    }
    response = p.send_message(params)
    #print str(response)
    return

#function for cleaning the phone number
def clean_phone_number(number):
    number = number.replace('"','')
    number = number.replace(' ','')
    number = number.lstrip('0') or '0'
    number = number.replace('-','')
    if '+91' in number:
        pass
    else:
        number = '+91' + number
    return number

class JsonResponse(HttpResponse):
    def __init__(self, content={}, mimetype=None, status=None,
             content_type='application/json'):
        super(JsonResponse, self).__init__(json.dumps(content), mimetype=mimetype,
                                           status=status, content_type=content_type)

def read_post_json(json_val):
    #json_val = json.dumps(json_val)
    return json.loads(json_val)

def generate_mobile_api_token(key):
    myvar = ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits + string.ascii_uppercase) for _ in range(16))
    myvar += str(key)
    ckey = hashlib.md5(myvar).hexdigest()
    if not ckey:
        return key
    else:
        return ckey

def generate_otp(n):
    return ''.join(["%s" % random.randint(0, 9) for num in range(0, n)])

def JsonQueryset(my_queryset):
    response_data = serializers.serialize("json",my_queryset)
    return HttpResponse(response_data, content_type='application/json')

def correction_in_time(time_obj):
    time_correct = time_obj + timedelta(minutes=330)
    return time_correct.strftime("%d %b %Y, %H:%M")


def bluedart_generate_waybill(order, waybill_form):
    url = "https://netconnect.bluedart.com/Ver1.7/ShippingAPI/WayBill/WayBillGeneration.svc?wsdl"
    binding.envns = ('SOAP-ENV', 'http://www.w3.org/2003/05/soap-envelope')
    client = Client(url, headers={'Content-Type': 'application/soap+xml; charset=utf-8'})
    wsans = ('wsa', "http://www.w3.org/2005/08/addressing")
    client.set_options(soapheaders = Element('Action', ns=wsans).setText('http://tempuri.org/IWayBillGeneration/GenerateWayBill'))
    # Setting up login profile
    profile =client.factory.create('Profile');
    profile.LicenceKey = "36d852d0cdc38f1e8200ef82f23243b0"
    profile.LoginID = "DL272366"
    profile.Version = "1.3"
    profile.Api_type = "S"
    # Setting up your request
    Request = client.factory.create('ns2:WayBillGenerationRequest')
    # Settinng up consignee in request
    consignee = Request.Consignee
    drop_address = order.drop_address
    consignee.ConsigneeAddress1 = drop_address.address_line[:30]
    if len(drop_address.address_line) > 30:
        consignee.ConsigneeAddress2 = drop_address.address_line[30:60]
    if len(drop_address.address_line) >60:
        consignee.ConsigneeAddress3 = drop_address.address_line[60:]
    consignee.ConsigneeName = drop_address.name
    consignee.ConsigneePincode = drop_address.pin_code
    consignee.ConsigneeMobile = drop_address.phone_number
    # Setting up service needed
    services = Request.Services
    services.ProductType.value = waybill_form.cleaned_data['package_type']
    services.ProductCode = "A"
    services.SubProductCode = waybill_form.cleaned_data['sub_product_type']
    services.PieceCount = 1
    services.ActualWeight = waybill_form.cleaned_data['actual_weight']
    services.CreditReferenceNo = str(uuid.uuid4().get_hex().upper()[0:15])
    if waybill_form.cleaned_data['sub_product_type'] == 'C':
        services.CollectableAmount = float(waybill_form.cleaned_data['collectable_amount'])
    services.DeclaredValue = float(waybill_form.cleaned_data['collectable_amount'])
    services.PickupDate = waybill_form.cleaned_data['pickup_date_time'].strftime("%Y-%m-%d")
    services.PickupTime = waybill_form.cleaned_data['pickup_date_time'].strftime("%H%M")
    services.Dimensions.Dimension = {
        'Breadth': waybill_form.cleaned_data['width'],
        'Length': waybill_form.cleaned_data['length'],
        'Height': waybill_form.cleaned_data['height'],
        'Count': 1
    }
    # Setting up shipper in request
    shipper = Request.Shipper
    pickup_address = order.pickup_address
    shipper.OriginArea = "DEL"
    shipper.CustomerAddress1 = "H.No - 2036/A"
    shipper.CustomerAddress2 = "Sec-3, Faridabad"
    shipper.CustomerAddress3 = "Near SRS value Bazaar"
    shipper.CustomerName = "PICKRR"
    shipper.CustomerPincode = "121004"
    shipper.IsToPayCustomer = False
    shipper.VendorCode = "PIC123"
    shipper.CustomerMobile = "919818197991"
    shipper.CustomerEmailID = "info@pickrr.com"
    shipper.CustomerCode = "272366"
    result = client.service.GenerateWayBill(Request, profile)
    return result


def random_password(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))