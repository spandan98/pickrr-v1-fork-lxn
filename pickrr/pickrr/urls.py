from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'pickrr.views.home', name='home'),
    url(r'^',include('pickup.urls',namespace='pickup')),
    url(r'^faqs/$', TemplateView.as_view(template_name='staticpages/faqs.html'), name='faq'),
    url(r'^users/',include('users.urls',namespace='users')),
    url(r'^order/',include('orders.urls',namespace='orders')),
    url(r'^tracking/',include('track.urls',namespace='track')),
    url(r'^api/',include('pickrr_common_api.urls',namespace='pickrr_common_api')),
    url(r'^price-calculator/',include('externalpriceapi.urls',namespace='externalpriceapi')),
    #url(r'^vendor-api/',include('vendorapi.urls',namespace='vendorapi')),
    url(r'^location/',include('locations.urls',namespace='locations')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^weshyp/admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^weshyp/admin/', include(admin.site.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
)
