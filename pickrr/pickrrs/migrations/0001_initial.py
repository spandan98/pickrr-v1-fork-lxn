# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pickrrs'
        db.create_table(u'pickrrs_pickrrs', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('email_id', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'pickrrs', ['Pickrrs'])


    def backwards(self, orm):
        # Deleting model 'Pickrrs'
        db.delete_table(u'pickrrs_pickrrs')


    models = {
        u'pickrrs.pickrrs': {
            'Meta': {'object_name': 'Pickrrs'},
            'email_id': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['pickrrs']