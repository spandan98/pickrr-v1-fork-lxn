from django.db import models

class Pickrrs(models.Model):
	name = models.CharField(max_length=128)
	email_id = models.EmailField(unique=True)
	phone_number = models.CharField(max_length=10)

	def __unicode__(self):
		return self.name