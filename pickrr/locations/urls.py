from django.conf.urls import patterns, url
from locations import views

urlpatterns = patterns('',
    url(r'^get-from-location-api/$',views.get_from_location_api,name='get_from_location_api'),
    url(r'^(?P<str_loc>.*)/get-to-location-api/$',views.get_to_location_api,name='get_to_location_api'),
)