from helpers.helpers import JsonResponse
from locations.models import City

def get_from_location_api(request):
	from_location_list = [ (from_city.id, str(from_city.name)) for from_city in City.objects.filter(from_service=True)]
	response_data = {}
	response_data['from_location'] = from_location_list
	return JsonResponse(response_data)

def get_to_location_api(request,str_loc):
	if str_loc == '':
		to_location_list = [ (to_city.id, str(to_city.name)) for to_city in City.objects.filter(to_service=True)]
	else:
		to_location_list = [ (to_city.id, str(to_city.name)) for to_city in City.objects.filter(to_service=True,name__icontains=str(str_loc))]
	response_data = {}
	response_data['to_location'] = to_location_list
	return JsonResponse(response_data)