# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'State'
        db.create_table(u'locations_state', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
        ))
        db.send_create_signal(u'locations', ['State'])

        # Adding model 'City'
        db.create_table(u'locations_city', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pickrr_zone', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['pricingmodel.PickrrZone'])),
            ('state', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['locations.State'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('from_service', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('to_service', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'locations', ['City'])

        # Adding model 'Region'
        db.create_table(u'locations_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['locations.City'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=128)),
        ))
        db.send_create_signal(u'locations', ['Region'])

        # Adding model 'Pincode'
        db.create_table(u'locations_pincode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('region', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['locations.Region'])),
            ('pincode', self.gf('django.db.models.fields.CharField')(max_length=6)),
        ))
        db.send_create_signal(u'locations', ['Pincode'])


    def backwards(self, orm):
        # Deleting model 'State'
        db.delete_table(u'locations_state')

        # Deleting model 'City'
        db.delete_table(u'locations_city')

        # Deleting model 'Region'
        db.delete_table(u'locations_region')

        # Deleting model 'Pincode'
        db.delete_table(u'locations_pincode')


    models = {
        u'locations.city': {
            'Meta': {'object_name': 'City'},
            'from_service': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'pickrr_zone': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['pricingmodel.PickrrZone']"}),
            'state': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['locations.State']"}),
            'to_service': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'locations.pincode': {
            'Meta': {'object_name': 'Pincode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pincode': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['locations.Region']"})
        },
        u'locations.region': {
            'Meta': {'object_name': 'Region'},
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['locations.City']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        },
        u'locations.state': {
            'Meta': {'object_name': 'State'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'})
        },
        u'pricingmodel.pickrrzone': {
            'Meta': {'object_name': 'PickrrZone'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'zone_name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        }
    }

    complete_apps = ['locations']