from django.db import models
from helpers.helpers import validate_pincode

class State(models.Model):
    name = models.CharField(max_length=128,unique=True)

    def __unicode__(self):
        return self.name

class City(models.Model):
    #only intercity zones. Intracity will be calculated on logic
    pickrr_zone = models.ForeignKey('pricingmodel.PickrrZone')
    state = models.ForeignKey(State)
    name = models.CharField(max_length=128)
    from_service = models.BooleanField(default=False)
    to_service = models.BooleanField(default=True)

    def __unicode__(self):
        return self.name

class Region(models.Model):
    city = models.ForeignKey(City)
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

class Pincode(models.Model):
    region = models.ForeignKey(Region)
    pincode = models.CharField(validators=[validate_pincode],max_length=6)

    def __unicode__(self):
        return self.pincode