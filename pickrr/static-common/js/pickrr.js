/*
Common javascript code for pickrr
*/
var ajaxobj, AjaxControl = {
    settings: {
        classAjaxPanel: ".panel-ajax",
        classAjaxForm: ".panel-ajax .form-ajax",
        classRadioCheckout: ".radio-checkout input",
        idLoadingRes: '#loading-resource',
        idPickrrHeading: '#pickrr-heading',
        idPickrrPanel: "#pickrr-panel"
    },
    init: function () {
        ajaxobj = this.settings;
        this.onAjaxFormSubmit();
        this.onOpenLoad();
        this.onOptionSelect();
        this.trackAjax();
    },
    onAjaxFormSubmit: function () {
        $(document).on('submit', ajaxobj.classAjaxForm, function(e) {
            e.preventDefault();
            AjaxControl.submitAjaxForm(this);
        });
    },
    onOpenLoad: function() {
        $('.modal').on('show.bs.modal', function(e) {
           var $modal = $(this),
           $ajaxPanel = $modal.find(ajaxobj.classAjaxPanel);
           if ($ajaxPanel.length !== 0 && $ajaxPanel.attr('data-state') === 'unloaded') {
            $.get($ajaxPanel.attr('data-url'), function(data) {
                $ajaxPanel.html(data);
                $ajaxPanel.attr('data-state', 'loaded');
                AjaxControl.fillFormData();
                Dropdown.initAllDropdown();
            });
        }
    });
    },
    onOptionSelect: function () {
        $(document).on('click', ajaxobj.classRadioCheckout, function(e){
            if (this.value === 'login_user') {
                $("#login-form").modal('show');
                if ($("#login_email").length !== 0) {
                    $("#login_email").val($("#id_email_field").val());
                }
                $("#btn-checkout").attr("disabled", "disabled");
            }
            if (this.value === 'register_user') {
                $("#signup-form").modal('show');
                if ($("#id_email").length !== 0) {
                    $("#id_email").val($("#id_email_field").val());
                }
                $("#btn-checkout").attr("disabled", "disabled");
            }
            if (this.value === "guest") {
                $("#btn-checkout").removeAttr("disabled");
            }
        });
    },
    trackAjax: function () {
        $(document).on('click', '.btn-track', function(e){
            $(e.target).button('loading');
            $.get($(e.target).attr('ajax-href'), function(data){
                $(e.target).parent().find('.track-data').html(data);
                 $(e.target).button('reset');
            });
        })
    },
    submitAjaxForm: function (form_el) {
        var additionFormData = AjaxControl.findAdditionalData(form_el);
        $.ajax({
            url: form_el.action,
            type: form_el.method,
            data: $(form_el).serialize() + additionFormData,
            beforeSend: function (xhr) {
                var loadContent = $(ajaxobj.idLoadingRes).html();
                $(form_el).parents('.panel-body').prepend(loadContent);
            },
            success: function (data, status, xhr) {
                var respType = xhr.getResponseHeader("content-type");
                if (respType === 'application/json') {
                    if (data.login === true) {
                        window.location.reload();
                    }
                }
                else {
                    if ($(form_el).parents(ajaxobj.idPickrrPanel).length !== 0) {
                        if ($(ajaxobj.idPickrrPanel).attr("data-position") === 'side' ) {
                            $(ajaxobj.idPickrrHeading).hide();
                            $(".more-trig").hide();
                            $("#how-work").hide();
                            $(ajaxobj.idPickrrPanel).addClass("col-md-12").removeClass("col-md-7");
                            $(ajaxobj.idPickrrPanel).attr("data-position", "center");
                        }
                        $(form_el).parents(ajaxobj.classAjaxPanel).html(data);
                    }
                    if($(data).find('form').attr('data-place') === 'main-panel'){
                        $(".modal").modal('hide');
                        $(ajaxobj.idPickrrPanel).find(ajaxobj.classAjaxPanel).html(data);
                    }
                    else {
                        $(form_el).parents(ajaxobj.classAjaxPanel).html(data);
                    }
                }
                Dropdown.initAllDropdown();
            },
            complete: function (xhr){
                $(ajaxobj.classAjaxPanel).find('.loading').remove();
            }
        });
    },
    fillFormData: function () {
        if ($("#login_email").length !== 0 && $("#id_email_field").length !== 0) {
            $("#login_email").val($("#id_email_field").val());
        }
        if ($("#id_email").length !== 0 && $("#id_email_field").length !== 0) {
            $("#id_email").val($("#id_email_field").val());
        }
    },
    findAdditionalData: function (form_el) {
        var button_name = $(form_el).find('button').attr('name');
        if (button_name !== '') {
            additionFormData = "&" + button_name;
        }
    return additionFormData;
    }
};

var dropobj, Dropdown = {
    settings: {
        classDropdown: ".pickrr-drop",
        classDropElem: ".drop-wrap li"
    },
    init: function () {
        dropobj = this.settings;
        this.initAllDropdown();
        this.onClickOpenCloseDrop();
        this.onChooseDrop();
        this.onClickSearchDrop();
        this.onSearchDrop();
        this.onChangeDrop();
    },
    initAllDropdown: function () {
        $(dropobj.classDropdown).each(function() {
            Dropdown.initDropdown($(this));
        });
    },
    initDropdown: function ($elem) {
        if (!$elem.is('select') || !$elem.hasClass("pickrr-drop") || $elem.hasClass("drop-hidden")) {
            return;
        }
        var $dropWrap = $("<div></div>").addClass("drop-wrap"),
        $dropUl = $('<ul></ul>'),
        selectVal = $elem.val(),
        $fakeInput = $("<input type=text readonly class='fakeinput form-control'>");
        if ($elem.find('option').length >= 15) {
            $fakeInput.removeAttr('readonly').addClass('searchinput').attr("type", "search");
            $dropUl.addClass("open-under");
        }
        $dropWrap.attr('data-target', "#" + $elem.attr('id'));
        $elem.find('option').each(function(){
            var $dropLi = $("<li></li>").addClass("visible").val(this.value);
            $dropLi.html(this.innerHTML);
            if (this.value === selectVal) {
                $dropLi.addClass('active');
                $fakeInput.val(this.innerHTML);
            }
            $dropUl.append($dropLi);
        });
        $dropWrap.append($fakeInput).append($dropUl);
        $elem.parent().append($dropWrap);
        $elem.addClass("drop-hidden");

    },
    onClickOpenCloseDrop: function() {
        $(document).on('click', function(e) {
            Dropdown.closeAllDrop();
            if ($(e.target).hasClass('fakeinput')) {
                $(e.target).parent('.drop-wrap').addClass('open');
            }
        });
    },
    onChooseDrop: function () {
        $(document).on('click', dropobj.classDropElem, function(){
            Dropdown.chooseThisDrop(this);
        });
    },
    onClickSearchDrop: function(){
        $(document).on('click', '.searchinput', function(){
            $(this).val("");
            var match_val = $(this).val().toUpperCase();
            $(this).parent('.drop-wrap').find("li").each(function(){
                if (this.innerHTML.indexOf(match_val) === -1) {
                    $(this).removeClass("visible");
                }else{
                    $(this).addClass("visible");
                }
            });
        });
    },
    onSearchDrop: function(){
        $(document).on('keyup', '.searchinput', function(e){
            var match_val = $(this).val().toUpperCase();
            $(this).parent('.drop-wrap').find("li").each(function(){
                if (this.innerHTML.indexOf(match_val) === -1) {
                    $(this).removeClass("visible");
                }else{
                    $(this).addClass("visible");
                }
            });
        });
        $(document).on('keydown', '.searchinput', function(e){
            if ((e.keyCode || e.which) === 13) {
                e.preventDefault();
                var chosenElem = $(this).parent('.drop-wrap').find('li.visible')[0];
                Dropdown.chooseThisDrop(chosenElem);
                $(this).parent('.drop-wrap').removeClass('open');
            }
        });
    },
    onChangeDrop: function() {
        $(document).on('change focusout', '.searchinput', function(e){
            var target = $(this).parent('.drop-wrap').attr('data-target');
            var searhVal = $(target).find('option:selected').html();
            $(this).val(searhVal);
        });
    },
    closeAllDrop: function () {
        $('.drop-wrap').removeClass('open');
    },
    chooseThisDrop: function(chosenElem) {
        var $parentWrap = $(chosenElem).parents('.drop-wrap');
        $parentWrap.find('li').removeClass('active');
        $(chosenElem).addClass('active');
        $parentWrap.find('.fakeinput').val(chosenElem.innerHTML);
        var target = $parentWrap.attr('data-target');
        $(target).val($(chosenElem).val());
    },

};
var landobj, LandControl = {
    settings: {
        classMoreTrig: ".more-trig"
    },
    init: function () {
        landobj = this.settings;
        this.onClickScroll();
    },
    onClickScroll: function () {
        $(document).on("click", landobj.classMoreTrig, function(){
            var target = $(this).attr("data-target");
            $("html, body").stop().animate({ scrollTop: $(target).offset().top }, 1000);
        });
    }
};
