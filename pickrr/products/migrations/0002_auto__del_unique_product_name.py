# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Product', fields ['name']
        db.delete_unique(u'products_product', ['name'])


    def backwards(self, orm):
        # Adding unique constraint on 'Product', fields ['name']
        db.create_unique(u'products_product', ['name'])


    models = {
        u'products.product': {
            'Meta': {'ordering': "['-pk']", 'object_name': 'Product'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '128'})
        }
    }

    complete_apps = ['products']