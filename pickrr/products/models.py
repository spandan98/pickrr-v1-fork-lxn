from django.db import models

#Products which will we will pickup at any instance of weshyp
class Product(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['-pk']

#specifications of the product to give our users an approximate idea of price. 
#This will be a direct effect on the user experience of weshyp
"""
class ProductSpecs(models.Model):
    product = models.ForeignKey(Product)
    spec = models.CharField(max_length=256)

    def __unicode__(self):
        my_var = str(self.spec) + '_' + str(self.product.name)
        return my_var

    class Meta:
        ordering = ['-pk']

class ProductWeight(models.Model):
    product = models.ForeignKey(Product,blank=True,null=True)
    product_spec = models.OneToOneField(ProductSpecs,blank=True,null=True)
    length = models.FloatField(verbose_name="Length (in cms)",default=0)
    breadth = models.FloatField(verbose_name="Breadth (in cms)",default=0)
    height = models.FloatField(verbose_name="Height (in cms)",default=0)
    weight = models.FloatField(verbose_name="Weight (in Kg)",default=0)
"""